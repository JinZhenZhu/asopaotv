package com.sts.asopao.Models;

import java.io.Serializable;

/**
 * Created by STS on 1/28/2018.
 */

public class VideoModel implements Serializable {

    int videoId = 0;
    String title = "", thumbnailUrl = "";

    public int getVideoId() {
        return videoId;
    }

    public void setVideoId(int videoId) {
        this.videoId = videoId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getThumbnailUrl() {
        return thumbnailUrl;
    }

    public void setThumbnailUrl(String thumbnailUrl) {
        this.thumbnailUrl = thumbnailUrl;
    }
}
