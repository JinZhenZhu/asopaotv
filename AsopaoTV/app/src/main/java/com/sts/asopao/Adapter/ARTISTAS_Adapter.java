package com.sts.asopao.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.sts.asopao.CustomView.SquareRoundedImageView;
import com.sts.asopao.Main.MainActivity;
import com.sts.asopao.Models.ARTISTAS_Model;
import com.sts.asopao.Models.ESTRENOS_Model;
import com.sts.asopao.R;

import java.util.ArrayList;

/**
 * Created by STS on 12/1/2017.
 */

public class ARTISTAS_Adapter extends BaseAdapter {

    MainActivity activity;

    ArrayList<ARTISTAS_Model> allData = new ArrayList<>();

    public ARTISTAS_Adapter(MainActivity activity) {
        this.activity = activity;
    }

    public void refresh(ArrayList<ARTISTAS_Model> data){
        allData.clear();
        allData.addAll(data);
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return allData.size();
    }

    @Override
    public ARTISTAS_Model getItem(int position) {
        return allData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        CustomHolder holder;
        if (convertView ==  null){
            holder =  new CustomHolder();
            LayoutInflater inflater = LayoutInflater.from(activity);
            convertView = inflater.inflate(R.layout.item_artistas, parent, false);
            holder.setId(convertView);
            convertView.setTag(holder);
        }else {
            holder = (CustomHolder)convertView.getTag();
        }

        ARTISTAS_Model data = allData.get(position);
        holder.setData(data);

        return convertView;
    }

    private class CustomHolder{

        SquareRoundedImageView imvPhoto;
        TextView txvArtist, txvChannelName;

        private void setId (View view){
            imvPhoto = (SquareRoundedImageView)view.findViewById(R.id.imvPhoto);
            txvArtist = (TextView)view.findViewById(R.id.txvArtist);
            txvChannelName = (TextView)view.findViewById(R.id.txvChannelName);
        }

        private void setData(ARTISTAS_Model model){
            if (model.getAvatar().length() > 0){
                Picasso.with(activity).load(model.getAvatar()).into(imvPhoto);

            }

            txvArtist.setText(model.getArtist());
            txvChannelName.setText(model.getChannelName());
        }
    }
}
