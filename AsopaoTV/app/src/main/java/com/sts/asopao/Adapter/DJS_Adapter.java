package com.sts.asopao.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.sts.asopao.CustomView.SquareRoundedImageView;
import com.sts.asopao.Main.MainActivity;
import com.sts.asopao.Models.DJS_Model;
import com.sts.asopao.Models.TOP_10_Model;
import com.sts.asopao.R;

import java.util.ArrayList;

/**
 * Created by STS on 12/1/2017.
 */

public class DJS_Adapter extends BaseAdapter {

    MainActivity activity;

    ArrayList<DJS_Model> allData = new ArrayList<>();

    public DJS_Adapter(MainActivity activity) {
        this.activity = activity;
    }

    public void refresh(ArrayList<DJS_Model> data){
        allData.clear();
        allData.addAll(data);
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return allData.size();
    }

    @Override
    public Object getItem(int position) {
        return allData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        CustomHolder holder;
        if (convertView ==  null){
            holder =  new CustomHolder();
            LayoutInflater inflater = LayoutInflater.from(activity);
            convertView = inflater.inflate(R.layout.item_djs, parent, false);
            holder.setId(convertView);
            convertView.setTag(holder);
        }else {
            holder = (CustomHolder)convertView.getTag();
        }

        DJS_Model data =  allData.get(position);
        holder.setData(data);

        return convertView;
    }

    private class CustomHolder{

        ImageView imvAvatar;
        TextView txvArtistName, txvChannelName ;

        private void setId(View view){
            imvAvatar = (ImageView)view.findViewById(R.id.imvAvatar);
            txvArtistName = (TextView)view.findViewById(R.id.txvArtistName);
            txvChannelName = (TextView)view.findViewById(R.id.txvChannelName);
        }

        private void setData (DJS_Model model){
            if (model.getAvatar().length() > 0){
                Picasso.with(activity).load(model.getAvatar()).into(imvAvatar);
            }

            txvArtistName.setText(model.getArtistName());
            txvChannelName.setText(model.getChannelName());
        }
    }
}
