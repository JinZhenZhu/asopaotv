package com.sts.asopao.Common;

/**
 * Created by STS on 2/5/2018.
 */

public class PrivacyTermsClass {

    public static final String  terms = "Welcome to AsopaoTV! In order for us to present our videos for you all to see and to keep the music evolution revolution in full swing, it’s important that we incorporate certain legal guidelines and ground rules to make sure everyone’s rights are respected and that we’re doing what we can to foster an environment that protects content owners’ rights and your personal privacy.\n" +
            "\n" +
            "TERMS OF USE AGREEMENT\n" +
            "\n" +
            "AsopaoTV is operated by AsopaoTV LLC and its affiliates (“Asopao”, “AsopaoTV”, “we”, “our” or “us”). Please read this Terms of Use Agreement (“Terms of Use”) carefully, as it constitutes legally binding terms and conditions and applies to your use of (a) the website located www.asopao.com and all corresponding web pages and websites associated with the foregoing URL (“Site”), (b) any other content, applications, features, functionality, information and services offered by us though the Site, including, without limitation, viral, embeddable or application/device-based features and related technology (e.g., mobile, web, console, desktop and other APIs, widgets, plugins, applications, etc.) (collectively, \"Services\"). These Terms of Use apply whether you are accessing the Services via a wireless or mobile device, a personal computer or any other technology or device (each, a “Device”). These Terms of Use do not cover other services, websites or any corresponding content, features, and activities made available by any other company or third party, unless specifically stated.\n" +
            "\n" +
            "These Terms of Use apply to all users of the Services, whether or not you have registered for same (“User”, “you” or “your”), and by using the Services you expressly understand, acknowledge and agree to comply with this Agreement and any additional terms and conditions that we may provide to you in connection with your use of or access to same, including, without limitation, in connection with related technology (e.g., widgets, plugins, applications, etc.) and other products and services we may offer or make available to you (“Additional Terms”). The Services may also provide rules of participation for certain activities and services, including, without limitation, contests, sweepstakes and other initiatives (“Rules”). The Services’ Privacy Policy (“Privacy Policy”), the Additional Terms and the Rules are hereby incorporated into these Terms of Use by reference as though fully set forth herein. To the extent that there is a conflict between these Terms of Use and the Additional Terms, the Additional Terms shall govern. To the extent that there is a conflict between these Terms of Use and the specific Rules for the activity in which you choose to participate, the Rules shall govern. To the extent that there is a conflict among the Terms of Use, the Additional Terms and/or the Rules, the following order of precedence shall apply: first, the Rules, second, the Additional Terms, and third, the Terms of Use.\n" +
            "\n" +
            "1.  SERVICES.\n" +
            "\n" +
            "A.   General.\n" +
            "Subject to the terms and conditions herein, the Services shall enable Users to access certain features, functionality, information and services provided by us and/or our affiliates, which may include, without limitation, providing Users with the ability to (i) access, view and embed certain audio-visual content (including, without limitation, certain music videos); (ii) create personal playlists and/or access playlists that have been created by other Users and (iii) access personalized information and content regarding music, artists and bands, such as recommendations, suggestions and notifications regarding audio-visual content, playlists and other related products and services.\n" +
            "\n" +
            "B.     Third Party Platforms.\n" +
            "Some of the Services may be dependent on and/or interoperate with third-party owned and/or operated platforms and services (e.g., Apple (iTunes, etc.), Facebook, Twitter, etc.) (each, a “Third Party Platform”) and may require that you be a registered member of such Third Party Platforms and provide certain account credentials and other information in order to access such Services. Such Third Party Platforms, in addition to providing application hosting, content distribution, support and other related services, may provide us with certain additional information about you, which may include, without limitation, your email address, legal name, country of residence, location, date of birth, music/artist/band and other preferences and usage data, all as more fully described herein and our access to, use of, and disclosure thereof shall be subject to the terms and conditions hereof, including the Privacy Policy.\n" +
            "\n" +
            "2.  ELIGIBILITY.\n" +
            "\n" +
            "By using the Services, you represent, warrant and covenant that (a) you are 13 years of age or older and reside in the United States of America, its territories and possessions (the “U.S.”), (b) your use of the Services does not violate (i) any applicable law, rule or regulation or (ii) any applicable terms, conditions or requirements promulgated by any provider of a Third Party Platform (e.g., Facebook’s Terms of Use, iTunes Store Terms of Use, etc.) and (c) all registration information you submit is truthful and accurate and you shall maintain and promptly update the accuracy of such information. Certain features of the Services may be subject to heightened age and/or other eligibility requirements and restrictions. If you are a user between the ages of 13 and 18, please review these Terms of Use with your parent or guardian. Your parent or guardian should agree to these Terms of Use on your behalf and parental discretion is advised for all users under the age of 18. Further, if you provide information that is untrue, inaccurate, not current or incomplete, or AsopaoTV suspects that such information is untrue, inaccurate, not current or incomplete, AsopaoTV has the right to suspend or terminate your registration (in whole or in part) and refuse any and all current or future use of the Services (or any portion thereof), in our sole discretion, with or without notice to you, and without liability or obligation to you or any third party.\n" +
            "\n" +
            "3.  MODIFICATIONS.\n" +
            "\n" +
            "We may modify these Terms of Use from time to time, and at any time, without notice to you, for any reason, in our sole discretion. We will post or display notices of material changes on the Services and/or notify you via other electronic means. The form of such notice is at our discretion. Once we post or make them available on the Services, these changes become effective immediately and if you use the Services after they become effective it will signify your agreement to be bound by the changes. We recommend that you check back frequently and review these Terms of Use regularly so you are aware of the most current rights and obligations that apply to you.\n" +
            "\n" +
            "4.  TERM.\n" +
            "\n" +
            "These Terms of Use, and any posted revision or modification thereto, shall remain in full force and effect while you use or are registered for the Services. You may terminate your use of or registration to the Services at any time, for any reason, and AsopaoTV may terminate your use of or registration to the Services at any time, for any or no reason, with or without prior notice or explanation, and without liability or obligation to you or any third party. Even after your registration is terminated, your obligations under these Terms of Use, the Privacy Policy, the Additional Terms and the Rules, including, without limitation, any indemnifications, warranties and limitations of liability, shall remain in effect.\n" +
            "\n" +
            "5.  REGISTRATION.\n" +
            "\n" +
            "In order to access and use certain content, features and functionality of the Services, we may require that you (a) register for the applicable Services, whether on the Site, a Third Party Platform or otherwise, including, in some instances, creating and/or providing a username and password combination (“User ID”) and (b) provide to us and/or make available (e.g., via Third Party Platform permissions and consents, etc.) certain additional information, which may include, without limitation, your email address, legal name, country of residence, location, date of birth, usage data and other information, and, for fee-based transactions and purchases (if applicable) offered by us, your physical address, telephone number(s), applicable payment information (e.g., payment card data, etc.) and other information (collectively, a “User Account”). If you elect to become a registered User of the Services, you are responsible for maintaining the strict confidentiality of your User ID, and you shall be responsible for any access to or use of the Services by you or any person or entity using your User ID, whether or not such access or use has been authorized by you or on your behalf, and whether or not such person or entity is your employee or agent. You agree to immediately notify AsopaoTV of any unauthorized use of your User ID or User Account or any other breach of security. It is your sole responsibility to (i) control the dissemination and use of your User ID and User Account, (ii) control access to your User ID and User Account, and (iii) cancel your User Account on the Services. We reserve the right to deny access, use and registration privileges to any User of the Services if we believe there is a question about the identity of the person trying to access any account or element of the Services. AsopaoTV shall not be responsible or liable for any loss or damage arising from your failure to comply with this Section 5.\n" +
            "\n" +
            "6.  USER CONDUCT.\n" +
            "\n" +
            "You are solely responsible for your conduct on and in connection with the Services. We want to keep the Services safe and fun for everyone and the use of the Services for unlawful or harmful activities is not allowed. You represent, warrant and agree that, while using the Services, you shall not:\n" +
            "\n" +
            "a)      intentionally or unintentionally engage in or encourage conduct that would violate any applicable local, state, national or international law, rule, regulation, judicial or government order or treaty or give rise to civil liability or violate or infringe upon any intellectual property, proprietary, privacy, moral, publicity or other rights of ours or of any other person or entity;\n" +
            "\n" +
            "b)      submit, post, email, display, transmit or otherwise make available on, through or in connection with the Services any material or take any action that is or is likely to be unlawful, harmful, threatening, abusive, tortious, defamatory, libelous, deceptive, fraudulent, invasive of another’s privacy or publicity rights, harassing, profane, obscene, vulgar or that contains explicit or graphic imagery, descriptions or accounts of excessive violence or sexual acts (including, without limitation, sexual language of a violent or threatening nature directed at another individual or group of individuals), contains a link to an adult website or is patently offensive, promotes racism, bigotry, hatred or physical harm of any kind against any group or individual;\n" +
            "\n" +
            "c)      submit, post, email, display, transmit or otherwise make available on, through or in connection with the Services any material that you do not have a right to make available under any law, rule or regulation or under contractual or fiduciary relationships (such as inside information, proprietary or confidential information learned or disclosed as part of employment relationships or under nondisclosure agreements), or otherwise creates a security or privacy risk for any other person or entity;\n" +
            "\n" +
            "d)      intentionally or unintentionally engage in or encourage conduct that affects adversely or reflect negatively on AsopaoTV or its affiliates, the Services, our goodwill, name or reputation or causes duress, distress or discomfort to us or anyone else, or discourage any person or entity from using all or any portion, features or functions of the Services, or from advertising, linking or becoming a supplier to us in connection with the Services;\n" +
            "\n" +
            "e)      submit, post, email, display, transmit or otherwise make available on, through or in connection with the Services any material that contains a software virus, worm, spyware, Trojan horse or other computer code, file or program designed to interrupt, impair, destroy or limit the functionality of any computer software or hardware or telecommunications equipment;\n" +
            "\n" +
            "f)       except as expressly permitted herein, use the Services for commercial or business purposes, including, without limitation, engaging in barter arrangements, pyramid schemes, advertising, marketing or offering goods or services or exploiting information or material obtained on, through or in connection with the Services, whether or not for financial or any other form of compensation or through linking with another website or web page;\n" +
            "\n" +
            "g)      modify, disrupt, impair, alter or interfere with the use, features, function, operation or maintenance of the Services or the rights or use or enjoyment of the Services by any other User;\n" +
            "\n" +
            "h)      impersonate any person or entity, including, without limitation, a AsopaoTV official, or falsely state or otherwise represent your affiliation with a person, entity or User Posting (as defined herein), transmit or otherwise make available on, through, or in connection with the Services false or misleading indications of origin, information or statements of fact;\n" +
            "\n" +
            "i)        forge headers or otherwise manipulate identifiers in order to disguise the origin of any content transmitted on, through or in connection with the Services, including User Postings (as defined herein); or\n" +
            "\n" +
            "j)        solicit passwords or personal identifying information for commercial or unlawful purposes from other Users, solicit Users with respect to their Content for commercial or unlawful purposes, or engage in spamming, flooding, harvesting of email addresses or other personal information, “spidering”, “screen scraping,”, “phishing”, “database scraping,” or any other activity with the purposes of obtaining lists of Users or other information; oruse or launch any automated systems, including, without limitation, “spiders,” “robots,” or “offline readers,” that access the Site and/or Services in a manner that sends more request messages to the AsopaoTV servers in one given period of time than a human being can reasonably send in the same period of time by using a conventional online web browser. Notwithstanding the foregoing, AsopaoTV grants the operators of public search engines permission to use spiders to copy materials from the site for the sole purpose of, and solely to the extent necessary for, creating publicly available searchable indices of the materials, but not caches or archives of such materials. AsopaoTV reserves the right to revoke these exceptions either generally or in specific cases.\n" +
            "\n" +
            "AsopaoTV reserves the right to investigate and take appropriate legal action against anyone who, in AsopaoTV’s sole discretion, violates, or is suspected of violating, this Section 6, including, without limitation, reporting you to law enforcement authorities. Further, you acknowledge, consent and agree that AsopaoTV may access, preserve and disclose your account and registration information and any other content or information if required to do so by law or if based on a good faith belief that such access, preservation or disclosure is reasonably necessary to (a) comply with the legal process; (b) enforce these Terms of Use; (c) respond to claims that any content or information violates the rights of any third party; (d) respond to your requests for customer or technical service; or (e) protect the rights, property or personal safety of AsopaoTV, its Users or any third parties.\n" +
            "\n" +
            "7.  PROPRIETARY RIGHTS.\n" +
            "\n" +
            "A.   As between you and AsopaoTV, AsopaoTV owns, solely and exclusively, all right, title and interest in and to the Services and all content contained and/or made available on, through or in connection therewith (the “Content”), and all such Content is protected, without limitation, under U.S. Federal and State, as well as applicable foreign laws, rules, regulations and treaties. The term “Content” includes, without limitation, all video (and the sound recordings and musical compositions embodied therein), artwork, photographs, illustrations, graphics, logos, copy, lyrics, text, computer code, data, user interfaces, visual interfaces, information, materials, and all copyrightable or otherwise legally protectable elements of the Services, including, without limitation, the design, selection, sequence, look and feel, and arrangement of the Services, and any copyrights, trademarks, service marks, trade names, trade dress, patent rights, database rights and/or other intellectual property and/or proprietary rights therein (including with respect to any content contained and/or made available in any advertisements or information presented to you via the Services). Unless the context clearly requires otherwise or we explicitly set forth in writing, the term “Services” includes “Content” as well.\n" +
            "\n" +
            "B.   Except as expressly set forth herein, the Services are to be used solely for your noncommercial, non-exclusive, non-assignable, non-transferable and limited personal use and for no other purposes. You must not alter, delete or conceal any copyright, trademark, service mark or other notices contained on the Services, including, without limitation, notices on any Content you transmit, display, print, stream or reproduce from the Services. Except as expressly authorized by AsopaoTV and set forth in Additional Terms (e.g., Services that allow for the use of embeddable or viral features, etc.), you shall not, nor shall you allow any third party (whether or not for your benefit or otherwise) to, reproduce, modify, create derivative works from, display, perform, publish, distribute, disseminate, broadcast or circulate to any third party (including, without limitation, on or via a third party website or platform), or otherwise use any Content without the express, prior written consent of AsopaoTV or its owner if AsopaoTV is not the owner. Any unauthorized or prohibited use of any Content may subject you to civil liability, criminal prosecution, or both, under applicable federal, state, local laws, or applicable foreign laws, rules, regulations and treaties. We require users to respect our copyrights, trademarks, and other intellectual property rights and shall aggressively enforce the same to the fullest extent of the law, including seeking criminal prosecution. We likewise respect the intellectual property of others. If you believe that the Services contains elements that infringe your copyrights in your work, please follow the procedures set forth in Section 8 below. Moreover, the framing or scraping of or in-line linking to the Services or any Content contained thereon and/or the use of webcrawler, spidering or other automated means to access, copy, index, process and/or store any Content made available on or through the Services other than as expressly authorized by us is prohibited. You further agree to abide by exclusionary protocols (e.g., Robot.txt, Automated Content Access Protocol (ACAP), etc.) used in connection with the Services.\n" +
            "\n" +
            "8.  DIGITAL MILLENNIUM COPYRIGHT ACT.\n" +
            "\n" +
            "A.   If you are a copyright owner or an agent thereof and believe that any content on the Services infringe upon your copyrights, you may submit a notification pursuant to the Digital Millennium Copyright Act (“DMCA”) by providing our Designated Agent (as set forth below) with the following information in writing (see 17 U.S.C. 512(c)(3) for further details):\n" +
            "\n" +
            "A physical or electronic signature of a person authorized to act on behalf of the owner of an exclusive right that is allegedly infringed;\n" +
            "\n" +
            "Identification of the copyrighted work claimed to have been infringed, or, if multiple copyrighted works on the Services are covered by a single notification, a representative list of such works on the Services;\n" +
            "\n" +
            "Identification of the material that is claimed to be infringing or to be the subject of infringing activity and that is to be removed or access to which is to be disabled and information reasonably sufficient to permit us to locate the material;\n" +
            "\n" +
            "Information reasonably sufficient to permit us to contact you, such as an address, telephone number, and, if applicable, e-mail address;\n" +
            "\n" +
            "A statement that you have a good faith belief that use of the material in the manner complained of is not authorized by the copyright owner, its agent or the law; and\n" +
            "\n" +
            "A statement that the information in the notification is accurate, and under penalty of perjury, that you are authorized to act on behalf of the owner of an exclusive right that is allegedly infringed.\n" +
            "\n" +
            "Written notification of claimed infringement must be submitted to the following Designated Agent:\n" +
            "\n" +
            "Name of Designated Agent: Nelson Bulider\n" +
            "\n" +
            "Address of Designated Agent:\n" +
            "AsopaoTV Copyright Agent\n" +
            "AsopaoTV LLC\n" +
            "P. O. Box 5551\n" +
            "Newark, NJ 07105\n" +
            "\n" +
            "Email Address of Designated Agent: buldier@gmail.com\n" +
            "\n" +
            "For clarity, only DMCA notices should be sent to the Designated Agent and any other feedback, comments, requests for technical support, and other communications should be directed to AsopaoTV customer service by sending an email to buldier@gmail.com. You acknowledge and agree that if you fail to comply with all of the requirements of this Section 8, your DMCA notice may not be valid.\n" +
            "\n" +
            "B.   If you believe that your content was removed (or to which access was disabled) is not infringing, or that you have the authorization from the copyright owner, the copyright owner’s agent, or pursuant to applicable law, to make such content available to AsopaoTV for use on the Services, you may send a counter-notice containing the following information to our Copyright Agent:\n" +
            "\n" +
            "Your physical or electronic signature;\n" +
            "\n" +
            "Identification of the content that has been removed or to which access has been disabled and the location at which the content appeared before it was removed or disabled;\n" +
            "\n" +
            "A statement that you have a good faith belief that the content was removed or disabled as a result of a mistake or a misidentification of the content; and\n" +
            "\n" +
            "Your name, address, telephone number, and, if applicable, e-mail address, and a statement that you shall accept service of process from the person who provided notification of the alleged infringement.\n" +
            "\n" +
            "If a counter-notice is received by our Copyright Agent, AsopaoTV may send a copy of the counter-notice to the original complaining party informing that person that it may replace the removed content or cease disabling it in 10 business days. Unless the copyright owner files an action seeking a court order against the content provider or the User, the removed content may be replaced, or access to it restored, in 10 to 14 business days or more after receipt of the counter-notice, at AsopaoTV’s sole discretion.\n" +
            "\n" +
            "9.  PERSONAL INFORMATION.\n" +
            "\n" +
            "We respect your privacy and the use and protection of your personally identifiable information. In the course of your use of the Services, you may be asked to provide certain personalized information to us or we may obtain such information from certain third parties, including, without limitation, Third Party Platforms (e.g., Facebook, etc.) (such information referred to hereinafter as “Personal Information”). Our information collection and use policies with respect to the privacy of such Personal Information are set forth in the Privacy Policy, which is incorporated herein by reference for all purposes. We encourage you to read the Privacy Policy, and to use it to help make informed decisions. You acknowledge and agree that you are solely responsible for the accuracy and content of Personal Information.\n" +
            "\n" +
            "10.  USER POSTINGS.\n" +
            "\n" +
            "A.   The Services may provide you and other Users with an opportunity to participate in blogs, web communities and other message, comment and communication features and may provide you with the opportunity to submit, post, email, display, transmit or otherwise make available photographs, graphics, comments, reviews, links, materials, ideas, opinions, messages, playlists and other information via the Services (each, a “User Posting”, and collectively, “User Postings”). When you submit User Postings you may also be asked to provide information about you and your submission. This may include, without limitation, such things as your User ID, a descriptive title, information about the User Posting, your location, activity, preferences and/or similar information. By submitting User Postings, you acknowledge and agree that the term “User Posting” also includes, without limitation, and refers to all of the information you submit or we may receive that is related to your User Posting.\n" +
            "\n" +
            "B.   You understand, acknowledge and agree that all User Postings are the sole responsibility of the person from which such User Postings originated. This means that you are solely and entirely responsible for the consequences of all User Postings that you submit, upload, post, email, display, transmit or otherwise make available. User Postings do not reflect the views of AsopaoTV and you understand that by using the Services, you may be exposed to other people’s User Postings that could be offensive, indecent or objectionable and, as such, AsopaoTV does not guarantee the accuracy, integrity, quality or content of any User Postings. Under no circumstances shall AsopaoTV be liable in any way for User Postings, including, without limitation, errors or omissions in any User Postings, or any loss or damage of any kind incurred as a result of any User Postings submitted, uploaded, posted, emailed, displayed, transmitted or otherwise made available.\n" +
            "\n" +
            "C.   The Services, including, without limitation, all User Posting features and functionality, is for non-commercial purposes only and you may not submit, post, email, display, transmit or otherwise make available, in any manner, any User Posting that we deem to be an Unauthorized Posting (as defined herein). We have the right, but not the obligation, to review any User Posting and to delete, remove, move, edit or reject, without notice to you, for any reason or for no reason whatsoever, any User Postings, including, without limitation, any Unauthorized Postings; provided, however, that AsopaoTV shall have no obligation or liability to you or any third party for failure to do so or for doing so in any particular manner. As used herein, the term “Unauthorized Posting” means any User Posting that is or may be construed as violating any of the terms and conditions of these Terms of Use, including, without limitation, Section 6 herein, or is deemed to be unacceptable to AsopaoTV, as determined in AsopaoTV’s sole discretion.\n" +
            "\n" +
            "D.   In connection with all User Postings you submit, post, email, display, transmit or otherwise make available, you grant to AsopaoTV the unqualified, unrestricted, unconditional, unlimited, worldwide, non-exclusive, irrevocable, perpetual and royalty-free right, license, authorization and permission, in any form or format, on or through any media or medium and with any technology or devices now known or hereafter developed or discovered, in whole or in part, to host, cache, store, maintain, use, reproduce, distribute, display, exhibit, perform, publish, broadcast, transmit, modify, prepare derivative works of, adapt, reformat, translate, and otherwise exploit all or any portion of your User Posting on the Services and any other websites, channels, services, and other distribution platforms, whether currently existing or developed in the future, for any purpose whatsoever (including, without limitation, for any promotional purposes) without accounting, notification, credit or other obligation to you, and the right to license and sub-license and authorize others to exercise any of the rights granted hereunder to AsopaoTV, in our sole discretion. For the avoidance of doubt, the rights, licenses and privileges described in these Terms of Use and granted to AsopaoTV shall commence immediately upon submission of your User Posting and shall continue thereafter perpetually and indefinitely, regardless of whether you use the Services as a registered User or not. AsopaoTV does not acquire any title or ownership rights in the User Postings that you submit and/or make available. After you submit, post, email, display, transmit or otherwise make available any User Posting, you continue to retain any such rights that you may have in such User Posting, subject to the rights, licenses and privileges granted herein. You also represent, warrant and covenant that (i) you own the User Posting posted by you or otherwise have the right to grant the rights, licenses and privileges described in these Terms of Use and to perform and comply with all of the requirements set forth herein; (ii) your submission, uploading, posting, emailing, displaying, transmission and/or making available of User Postings does not violate these Terms of Use, any rights of any other party or entity, any of your obligations, any law, rule or regulation or infringe upon, misappropriate or violate any intellectual property, proprietary, privacy, moral, publicity or other rights of any party or entity; (iii) you are not a minor and you have the legal right and capability to enter into these Terms of Use and perform and comply with all of its terms and conditions or, if you are a minor, your parent or legal guardian has agreed to these Terms of Use on your behalf; and (iv) you hold and shall continue to hold all the ownership, license, proprietary and other rights necessary to enter into, authorize, grant rights and perform your obligations under these Terms of Use and shall pay for all royalties, fees, and any other monies owing to any person or entity by reason of your User Postings.\n" +
            "\n" +
            "E.   You acknowledge that other persons may have submitted User Postings to us, may have made public or developed, or may originate, submit, make public or develop, material similar or identical to all or a portion of your User Postings or concepts contained therein, and you understand and agree that you shall not be entitled to any compensation because of the use or exploitation thereof and the submission of User Postings, or any posting or display thereof, is not any admission of novelty, priority or originality. Even if you subsequently see or learn of a presentation, sound recording, composition, demo, idea, script, drawing, motion picture, photograph, film, video or any other content which appears to incorporate any idea or concept or include anything similar or identical to that contained in any User Posting you or anyone else submits, that is purely coincidental and unavoidable.\n" +
            "\n" +
            "11.  ADVERTISEMENTS.\n" +
            "\n" +
            "From time to time, you may choose to communicate with, interact with, or obtain Third Party Services (as such term is defined herein) of or from advertisers, sponsors, or promotional partners (collectively, the “Advertisers”) found on or through the Services or a hyperlinked site, service or platform. All such communication, interaction and participation is strictly and solely between you and such Advertisers and we shall not be responsible or liable to you in any way in connection with these activities or transactions (including, without limitation, any representations, warranties, covenants, contracts or other terms or conditions that may exist between you and the Advertisers or any goods or services you may purchase or obtain from any Advertiser).\n" +
            "\n" +
            "12.  PROMOTIONS.\n" +
            "\n" +
            "From time to time, AsopaoTV, or the Services’ operational service providers, suppliers, and Advertisers may conduct promotions on, through or in connection with the Services, including, without limitation, contests and sweepstakes (collectively, the “Promotions”). Each Promotion may have Additional Terms and/or Rules which shall be posted or otherwise made available to you and, for purposes of each Promotion, shall be deemed incorporated into and form a part of these Terms of Use.\n" +
            "\n" +
            "13.  FEES.\n" +
            "\n" +
            "We may make certain fee-based products and/or services available to Users of the Services. For example, you may be able to order certain music-related products and/or licenses through the Services. Additional Terms may apply to your use of, access to and purchase of such fee-based products or services and such Additional Terms are incorporated herein by reference. You may only purchase such fee-based products and/or services if, and you hereby represent, warrant and agree that (a) you are 18 years of age or older and a resident of the U.S. and (b) you shall pay in full the prices and fees (including, without limitation, all applicable taxes) for any purchases you, or anyone using your User Account, make via credit, debit or charge card or other payment means acceptable to AsopaoTV concurrent with your online order. If payment is not received by us from your credit, debit or charge card issuer or its agents or other payment service provider, you agree to promptly pay all amounts due upon demand by us. AsopaoTV may revise any or all of the fees and prices associated with the fee-based products and/or services at any time for any or no reason. Further, AsopaoTV does not guarantee that product descriptions or other content and products will be available, accurate, complete, reliable, current or error-free. Descriptions and images of, and references to, products or services do not imply our or any of our affiliates' endorsement of such products or services. Moreover, AsopaoTV and its third party operational service providers reserve the right, with or without prior notice, for any or no reason, to change product descriptions, images, and references; to limit the available quantity of any product; to honor, or impose conditions on the honoring of, any coupon, coupon code, promotional code or other similar promotions; to bar any user from conducting any or all transaction(s); and/or to refuse to provide any User with any product. Further, if AsopaoTV terminates your use of or registration to the Services because you have breached these Terms of Use, you shall not be entitled to a refund of any unused portion of any fees, payments or other consideration.\n" +
            "\n" +
            "14.  THIRD PARTY PLATFORMS, SERVICES AND CONTENT.\n" +
            "\n" +
            "The appearance, availability, or your use of (a) URLs or hyperlinks referenced or included anywhere in connection with the Services or any other form of link or re-direction of your connection to, with or through the Services, or (b) any third party websites, content, data, information, applications, platforms, goods, services or materials, including Third Party Platforms (collectively, “Third Party Services”) does not constitute an endorsement by, nor does it incur any obligation, responsibility or liability on the part of AsopaoTV, its affiliates, or any of their respective successors and assigns, directors, officers, employees, representatives, agents, licensors, Advertisers, suppliers, partners or service providers. We do not verify, endorse, or have any responsibility for Third Party Services and any third party business practices (including, without limitation, their privacy policies), whether the Services', AsopaoTV’s or its affiliates’ logos, marks, names and/or sponsorship or other identification is on the Third Party Services. If any Third Party Services you interact with obtains or collects personal information from you, in no event shall we assume or have any responsibility or liability in connection for any use, collection or disclosure by or in connection with such Third Party Services. Accordingly, we encourage you to be aware when you leave the Services and to read the terms and conditions and privacy policy of each Third Party Service you use.\n" +
            "\n" +
            "15.  DATA AND WIRELESS ACCESS CHARGES.\n" +
            "\n" +
            "Certain AsopaoTV Apps and other Services may require data access, and the provider of data access (e.g., network operator, wireless carrier, etc.) for your Device may charge you data access fees in connection with your use of such AsopaoTV Apps and other Services, including, without limitation, wireless carrier messaging and other communication, messaging and data fees and charges. Under no circumstances will AsopaoTV be responsible for any such data access fees and charges in connection with your use of any AsopaoTV Apps or other Services, including wireless internet, email, text messaging or other charges or fees incurred by you (or any person that has access to your Device, telephone number, email address, User Account or other similar information). Further, the use or availability of certain AsopaoTV Apps and other Services may be prohibited or restricted by your wireless carrier and/or data access provider, and not all AsopaoTV Apps and other Services may work with all wireless carriers, networks, platforms, services or Devices.\n" +
            "\n" +
            "16.  ASSIGNMENT.\n" +
            "\n" +
            "These Terms of Use, and any rights, licenses and privileges granted herein, may not be transferred or assigned by you, but may be assigned or transferred by AsopaoTV without restriction and without notice to you.\n" +
            "\n" +
            "17.  INDEMNITY.\n" +
            "\n" +
            "You agree to defend, indemnify and hold AsopaoTV, its successors and assigns, directors, officers, employees, representatives, agents, licensors, Advertisers, suppliers and operational service providers harmless from any and all claims, liabilities, damages, losses, costs and expenses (including reasonable attorneys’ fees), arising in any way out of or in connection with (a) your use of the Services, (b) your breach or violation these Terms of Use or (c) your User Postings. AsopaoTV reserves the right, at its own expense, to assume the exclusive defense and control of any matter otherwise subject to indemnification by you and all negotiation for its settlement or compromise (as applicable), and in each such case, you agree to fully cooperate with us upon our request.\n" +
            "\n" +
            "18.  DISCLAIMER AND LIMITATIONS OF LIABILITY.\n" +
            "\n" +
            "THESE SERVICES, AND ALL CONTENT, PRODUCTS, SERVICES AND USER POSTINGS MADE AVAILABLE ON, THROUGH OR IN CONNECTION THEREWITH, ARE MADE AVAILABLE ON AN “AS IS” AND “AS AVAILABLE” BASIS, WITHOUT ANY REPRESENTATION OR WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, OR ANY GUARANTY OR ASSURANCE THE SERVICES WILL BE AVAILABLE FOR USE, OR THAT ALL PRODUCTS, FEATURES, FUNCTIONS, SERVICES OR OPERATIONS WILL BE AVAILABLE OR PERFORM AS DESCRIBED. WITHOUT LIMITING THE FOREGOING, WE ARE NOT RESPONSIBLE OR LIABLE FOR ANY MALICIOUS CODE, DELAYS, INACCURACIES, ERRORS, OR OMISSIONS ARISING OUT OF YOUR USE OF THE SERVICES. YOU UNDERSTAND, ACKNOWLEDGE AND AGREE THAT YOU ARE ASSUMING THE ENTIRE RISK AS TO THE QUALITY, ACCURACY, PERFORMANCE, TIMELINESS, ADEQUACY, COMPLETENESS, CORRECTNESS, AUTHENTICITY, SECURITY AND VALIDITY OF ANY AND ALL FEATURES AND FUNCTIONS OF THE SERVICES, INCLUDING, WITHOUT LIMITATION, USER POSTINGS AND CONTENT ASSOCIATED WITH YOUR USE OF THE SERVICES.\n" +
            "\n" +
            "YOU UNDERSTAND AND AGREE THAT, TO THE FULLEST EXTENT PERMISSIBLE BY LAW, ASOPAOTV, ITS AFFILIATES, SUCCESSORS AND ASSIGNS, OFFICERS, DIRECTORS, EMPLOYEES, AGENTS, REPRESENTATIVES, LICENSORS, OPERATIONAL SERVICE PROVIDERS, ADVERTISERS AND SUPPLIERS, SHALL NOT BE LIABLE FOR ANY LOSS OR DAMAGE, OF ANY KIND, DIRECT OR INDIRECT, IN CONNECTION WITH OR ARISING FROM USE OF THE SERVICES OR FROM THESE TERMS OF USE, INCLUDING, WITHOUT LIMITATION, COMPENSATORY, CONSEQUENTIAL, INCIDENTAL, INDIRECT, SPECIAL OR PUNITIVE DAMAGES.\n" +
            "\n" +
            "YOU FURTHER UNDERSTAND AND ACKNOWLEDGE THE CAPACITY OF THE SERVICES, IN THE AGGREGATE AND FOR EACH USER, IS LIMITED. CONSEQUENTLY, SOME MESSAGES, CONTENT AND TRANSMISSIONS, INCLUDING, WITHOUT LIMITATION, USER POSTINGS, MAY NOT BE PROCESSED IN A TIMELY FASHION OR AT ALL, AND SOME FEATURES OR FUNCTIONS MAY BE RESTRICTED OR DELAYED OR BECOME COMPLETELY INOPERABLE. AS A RESULT, YOU ACKNOWLEDGE AND AGREE THAT ASOPAOTV ASSUMES NO LIABILITY, RESPONSIBILITY OR OBLIGATION TO TRANSMIT, PROCESS, STORE, RECEIVE OR DELIVER TRANSACTIONS OR USER POSTINGS OR FOR ANY FAILURE OR DELAY ASSOCIATED WITH ANY USER POSTINGS AND YOU ARE HEREBY EXPRESSLY ADVISED NOT TO RELY UPON THE TIMELINESS OR PERFORMANCE OF THE SERVICES FOR ANY TRANSACTIONS OR USER POSTINGS. SOME JURISDICTIONS DO NOT ALLOW FOR THE EXCLUSION OF CERTAIN WARRANTIES OR CERTAIN LIMITATIONS ON DAMAGES AND REMEDIES, ACCORDINGLY SOME OF THE EXCLUSIONS AND LIMITATIONS DESCRIBED IN THESE TERMS OF USE MAY NOT APPLY TO YOU.\n" +
            "\n" +
            "19.  CUSTOMER SUPPORT.\n" +
            "\n" +
            "For assistance with technical issues or customer support inquiries, please send an email to buldier@gmail.com.\n" +
            "\n" +
            "20.  GOVERNING LAW; MISCELLANEOUS.\n" +
            "\n" +
            "A.   These Terms of Use, together with any Additional Terms, Rules, our Privacy Policy, and any other regulations, procedures and policies which we refer to and which are hereby incorporated by reference, contain the entire understanding and agreement between you and AsopaoTV concerning the Services and your use thereof, and supersedes any and all prior or inconsistent understandings relating to the Services and your use thereof. These Terms of Use cannot be changed or terminated orally. If any provision of these Terms of Use is held to be illegal, invalid or unenforceable, this shall not affect any other provisions and these Terms of Use shall be deemed amended to the extent necessary to make it legal, valid and enforceable. Any provision which must survive in order to allow us to enforce its meaning shall survive the termination of these Terms of Use; however, no action arising out of these Terms of Use or your use of the Services, regardless of form or the basis of the claim, may be brought by you more than one year after the cause of action has arisen (or if multiple causes, from the date the first such cause arose).\n" +
            "\n" +
            "B.   These Terms of Use and your use of the Services is governed by, construed and enforced in accordance with the internal substantive laws of the State of New York (notwithstanding the state’s conflict of laws provisions) applicable to contracts made, executed and wholly performed in New York, and, for the purposes of any and all legal or equitable actions, you specifically agree and submit to the exclusive jurisdiction and venue of the State and Federal Courts situated in the State and County of New York and agree you shall not object to such jurisdiction or venue on the grounds of lack of personal jurisdiction, forum non conveniens or otherwise.\n" +
            "\n" +
            "C.   IN ANY ACTION OR PROCEEDING COMMENCED TO ENFORCE ANY RIGHT OR OBLIGATION OF THE PARTIES UNDER THIS AGREEMENT, YOUR USE OF THE SERVICES OR WITH RESPECT TO THE SUBJECT MATTER HEREOF, YOU HEREBY WAIVE ANY RIGHT YOU MAY NOW HAVE OR HEREAFTER POSSESS TO A TRIAL BY JURY.\n" +
            "\n" +
            "These Terms of Use were last modified on 20th January 2016 and are effective immediately.";

    public static final String policy = "This privacy policy has been compiled to better serve those who are concerned with how their 'Personally identifiable information' (PII) is being used online. PII, as used in US privacy law and information security, is information that can be used on its own or with other information to identify, contact, or locate a single person, or to identify an individual in context. Please read our privacy policy carefully to get a clear understanding of how we collect, use, protect or otherwise handle your Personally Identifiable Information in accordance with our website.\n" +
            "\n" +
            "What personal information do we collect from the people that visit our blog, website or app?\n" +
            "\n" +
            "When ordering or registering on our site, as appropriate, you may be asked to enter your email address or other details to help you with your experience.\n" +
            "\n" +
            "When do we collect information?\n" +
            "\n" +
            "We collect information from you when you register on our site, fill out a form or enter information on our site.\n" +
            "\n" +
            "\n" +
            "How do we use your information?\n" +
            "\n" +
            "We may use the information we collect from you when you register, make a purchase, sign up for our newsletter, respond to a survey or marketing communication, surf the website, or use certain other site features in the following ways:\n" +
            "\n" +
            "      • To personalize user's experience and to allow us to deliver the type of content and product offerings in which you are most interested.\n" +
            "      • To improve our website in order to better serve you.\n" +
            "      • To allow us to better service you in responding to your customer service requests.\n" +
            "      • To administer a contest, promotion, survey or other site feature.\n" +
            "      • To send periodic emails regarding your order or other products and services.\n" +
            "\n" +
            "How do we protect visitor information?\n" +
            "\n" +
            "We do not use vulnerability scanning and/or scanning to PCI standards.\n" +
            "We do not use Malware Scanning.\n" +
            "\n" +
            "We do not use an SSL certificate\n" +
            "      • We do not need an SSL because:\n" +
            "we do not processes sensitive information such as credit card numbers\n" +
            "\n" +
            "Do we use 'cookies'?\n" +
            "\n" +
            "Yes. Cookies are small files that a site or its service provider transfers to your computer's hard drive through your Web browser (if you allow) that enables the site's or service provider's systems to recognize your browser and capture and remember certain information. For instance, we use cookies to help us remember and process the items in your shopping cart. They are also used to help us understand your preferences based on previous or current site activity, which enables us to provide you with improved services. We also use cookies to help us compile aggregate data about site traffic and site interaction so that we can offer better site experiences and tools in the future.\n" +
            "\n" +
            "We use cookies to:\n" +
            "      • Understand and save user's preferences for future visits.\n" +
            "      • Compile aggregate data about site traffic and site interactions in order to offer better site experiences and tools in the future. We may also use trusted third-party services that track this information on our behalf.\n" +
            "\n" +
            "You can choose to have your computer warn you each time a cookie is being sent, or you can choose to turn off all cookies. You do this through your browser (like Internet Explorer) settings. Each browser is a little different, so look at your browser's Help menu to learn the correct way to modify your cookies.\n" +
            "\n" +
            "If users disable cookies in their browser:\n" +
            "\n" +
            "If you disable cookies off, some features will be disabled It will turn off some of the features that make your site experience more efficient and some of our services will not function properly.\n" +
            "\n" +
            "However, you can still place orders\n" +
            "Authentication\n" +
            ".\n" +
            "\n" +
            "\n" +
            "Third-party disclosure\n" +
            "\n" +
            "We do not sell, trade, or otherwise transfer to outside parties your personally identifiable information unless we provide users with advance notice. This does not include website hosting partners and other parties who assist us in operating our website, conducting our business, or serving our users, so long as those parties agree to keep this information confidential. We may also release information when it's release is appropriate to comply with the law, enforce our site policies, or protect ours or others' rights, property, or safety. \n" +
            "\n" +
            "However, non-personally identifiable visitor information may be provided to other parties for marketing, advertising, or other uses.\n" +
            "\n" +
            "Third-party links\n" +
            "\n" +
            "Occasionally, at our discretion, we may include or offer third-party products or services on our website. These third-party sites have separate and independent privacy policies. We therefore have no responsibility or liability for the content and activities of these linked sites. Nonetheless, we seek to protect the integrity of our site and welcome any feedback about these sites.\n" +
            "\n" +
            "Google\n" +
            "\n" +
            "Google's advertising requirements can be summed up by Google's Advertising Principles. They are put in place to provide a positive experience for users. https://support.google.com/adwordspolicy/answer/1316548?hl=en \n" +
            "\n" +
            "We use Google AdSense Advertising on our website.\n" +
            "\n" +
            "Google, as a third-party vendor, uses cookies to serve ads on our site. Google's use of the DART cookie enables it to serve ads to our users based on previous visits to our site and other sites on the Internet. Users may opt-out of the use of the DART cookie by visiting the Google Ad and Content Network privacy policy.\n" +
            "\n" +
            "We have implemented the following:\n" +
            "      • Demographics and Interests Reporting\n" +
            "\n" +
            "We along with third-party vendors, such as Google use first-party cookies (such as the Google Analytics cookies) and third-party cookies (such as the DoubleClick cookie) or other third-party identifiers together to compile data regarding user interactions with ad impressions and other ad service functions as they relate to our website.\n" +
            "\n" +
            "Opting out:\n" +
            "Users can set preferences for how Google advertises to you using the Google Ad Settings page. Alternatively, you can opt out by visiting the Network Advertising initiative opt out page or permanently using the Google Analytics Opt Out Browser add on.\n" +
            "\n" +
            "California Online Privacy Protection Act\n" +
            "\n" +
            "CalOPPA is the first state law in the nation to require commercial websites and online services to post a privacy policy. The law's reach stretches well beyond California to require a person or company in the United States (and conceivably the world) that operates websites collecting personally identifiable information from California consumers to post a conspicuous privacy policy on its website stating exactly the information being collected and those individuals with whom it is being shared, and to comply with this policy. - See more at: http://consumercal.org/california-online-privacy-protection-act-caloppa/#sthash.0FdRbT51.dpuf\n" +
            "\n" +
            "According to CalOPPA we agree to the following:\n" +
            "Users can visit our site anonymously.\n" +
            "Once this privacy policy is created, we will add a link to it on our home page or as a minimum on the first significant page after entering our website.\n" +
            "Our Privacy Policy link includes the word 'Privacy' and can be easily be found on the page specified above.\n" +
            "\n" +
            "Users will be notified of any privacy policy changes:\n" +
            "      • On our Privacy Policy Page\n" +
            "Users are able to change their personal information:\n" +
            "      • By logging in to their account\n" +
            "\n" +
            "How does our site handle do not track signals?\n" +
            "We honor do not track signals and do not track, plant cookies, or use advertising when a Do Not Track (DNT) browser mechanism is in place.\n" +
            "\n" +
            "Does our site allow third-party behavioral tracking?\n" +
            "It's also important to note that we allow third-party behavioral tracking\n" +
            "\n" +
            "COPPA (Children Online Privacy Protection Act)\n" +
            "\n" +
            "When it comes to the collection of personal information from children under 13, the Children's Online Privacy Protection Act (COPPA) puts parents in control. The Federal Trade Commission, the nation's consumer protection agency, enforces the COPPA Rule, which spells out what operators of websites and online services must do to protect children's privacy and safety online.\n" +
            "\n" +
            "We do not specifically market to children under 13.\n" +
            "\n" +
            "Fair Information Practices\n" +
            "\n" +
            "The Fair Information Practices Principles form the backbone of privacy law in the United States and the concepts they include have played a significant role in the development of data protection laws around the globe. Understanding the Fair Information Practice Principles and how they should be implemented is critical to comply with the various privacy laws that protect personal information.\n" +
            "\n" +
            "In order to be in line with Fair Information Practices we will take the following responsive action, should a data breach occur:\n" +
            "We will notify the users via email\n" +
            "      • Within 7 business days\n" +
            "We will notify the users via in-site notification\n" +
            "      • Within 7 business days\n" +
            "\n" +
            "We also agree to the Individual Redress Principle, which requires that individuals have a right to pursue legally enforceable rights against data collectors and processors who fail to adhere to the law. This principle requires not only that individuals have enforceable rights against data users, but also that individuals have recourse to courts or government agencies to investigate and/or prosecute non-compliance by data processors.\n" +
            "\n" +
            "CAN SPAM Act\n" +
            "\n" +
            "The CAN-SPAM Act is a law that sets the rules for commercial email, establishes requirements for commercial messages, gives recipients the right to have emails stopped from being sent to them, and spells out tough penalties for violations.\n" +
            "\n" +
            "We collect your email address in order to:\n" +
            "      • Send information, respond to inquiries, and/or other requests or questions.\n" +
            "      • Market to our mailing list or continue to send emails to our clients after the original transaction has occurred.\n" +
            "\n" +
            "To be in accordance with CANSPAM we agree to the following:\n" +
            "      • NOT use false or misleading subjects or email addresses.\n" +
            "      • Identify the message as an advertisement in some reasonable way.\n" +
            "      • Include the physical address of our business or site headquarters.\n" +
            "      • Monitor third-party email marketing services for compliance, if one is used.\n" +
            "      • Honor opt-out/unsubscribe requests quickly.\n" +
            "      • Allow users to unsubscribe by using the link at the bottom of each email.\n" +
            "\n" +
            "If at any time you would like to unsubscribe from receiving future emails, you can email us at\n" +
            "      • Follow the instructions at the bottom of each email.\n" +
            "and we will promptly remove you from ALL correspondence.\n" +
            "\n" +
            "\n" +
            "\n" +
            "Contacting Us\n" +
            "If there are any questions regarding this privacy policy you may contact us using the information below.\n" +
            "\n" +
            "asopao.com\n" +
            "P. O. Box 5551\n" +
            "Newark, NJ 07105\n" +
            "\n" +
            "USA\n" +
            "info@asopao.com";
}
