package com.sts.asopao.Fragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.github.paolorotolo.expandableheightlistview.ExpandableHeightListView;
import com.sts.asopao.APIs.GetTop10VideosRequest;
import com.sts.asopao.Adapter.Top10Adapter;
import com.sts.asopao.Adapter.VISTO_AHORA_Adapter;
import com.sts.asopao.Base.BaseFragment;
import com.sts.asopao.Common.Commons;
import com.sts.asopao.Common.Constants;
import com.sts.asopao.Common.ReqConst;
import com.sts.asopao.Main.MainActivity;
import com.sts.asopao.Models.Top10VideoModel;
import com.sts.asopao.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by STS on 12/1/2017.
 */

@SuppressLint("ValidFragment")
public class Top10Detail_Fragment extends BaseFragment{

    MainActivity activity;
    Top10Adapter adapter;

    ExpandableHeightListView lstTop10;
    TextView txvDateRange;
    int channelId = 0;

    public Top10Detail_Fragment(MainActivity activity, int channelId) {
        this.activity = activity;
        this.channelId = channelId;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View fragment = inflater.inflate(R.layout.fragment_top_10_detail, container, false);

        adapter = new Top10Adapter(activity);
        lstTop10 = (ExpandableHeightListView)fragment.findViewById(R.id.lstTop10);
        lstTop10.setAdapter(adapter);
        lstTop10.setExpanded(true);
        lstTop10.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Commons.selectedVideoId = ((Top10VideoModel)adapter.getItem(position)).getVideoId();
                activity.showVideoMusicalFragment();
            }
        });

        txvDateRange = (TextView)fragment.findViewById(R.id.txvDateRange);

        getAllData();

        return fragment;
    }

    private void getAllData() {
        final Response.Listener<String> res = new Response.Listener<String>() {
            @Override
            public void onResponse(String json) {
                activity.closeProgress();
                try{
                    JSONObject response = new JSONObject(json);
                    int resultCode = response.getInt(ReqConst.RES_CODE);
                    if (resultCode == ReqConst.CODE_SUCCESS){

                        txvDateRange.setText(response.getString(ReqConst.DATE_RANGE));

                        ArrayList<Top10VideoModel> mTop10VideoModelList =  new ArrayList<>();
                        JSONArray jsonTop10VideoModelList = response.getJSONArray(ReqConst.VIDEOS);

                        for (int i = 0; i < jsonTop10VideoModelList.length(); i++){

                            Top10VideoModel mTop10VideoModel = new Top10VideoModel();
                            JSONObject jsonTop10TabModel = (JSONObject)jsonTop10VideoModelList.get(i);

                            mTop10VideoModel.setVideoId(jsonTop10TabModel.getInt(ReqConst.VIDEO_ID));
                            mTop10VideoModel.setTitle(jsonTop10TabModel.getString(ReqConst.TITLE));
                            mTop10VideoModel.setChannelId(jsonTop10TabModel.getString(ReqConst.CHANNEL_ID));
                            mTop10VideoModel.setNoOfVotes(jsonTop10TabModel.getString(ReqConst.NO_OF_VOTES));
                            mTop10VideoModel.setThumbnailUrl(jsonTop10TabModel.getString(ReqConst.THUMBNAIL_URL));

                            mTop10VideoModelList.add(mTop10VideoModel);
                        }

                        adapter.refresh(mTop10VideoModelList);

                    }else {
                        activity.showAlertDialog("Email or Password Error!");
                    }
                }catch (JSONException e){
                    activity.showAlertDialog(getString(R.string.server_failed));
                }
            }
        };
        final Response.ErrorListener error = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                activity.closeProgress();
                activity.showAlertDialog(getString(R.string.server_failed));
            }
        };

        activity.showProgress();
        GetTop10VideosRequest req = new GetTop10VideosRequest(String.valueOf(channelId), res, error);
        req.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue request = Volley.newRequestQueue(activity);
        request.add(req);
    }
}
