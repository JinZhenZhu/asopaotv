package com.sts.asopao.Main;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.astuetz.PagerSlidingTabStrip;
import com.kobakei.ratethisapp.RateThisApp;
import com.makeramen.roundedimageview.RoundedImageView;
import com.squareup.picasso.Picasso;
import com.sts.asopao.Adapter.MainAdapter;
import com.sts.asopao.AsopaoTVApplication;
import com.sts.asopao.Base.BaseActivity;
import com.sts.asopao.Common.Commons;
import com.sts.asopao.Common.Constants;
import com.sts.asopao.Common.ReqConst;
import com.sts.asopao.CustomView.SquareImageView;
import com.sts.asopao.Fragment.ArtistasDetailFragment;
import com.sts.asopao.Fragment.GenerosDetailFragment;
import com.sts.asopao.Fragment.Top10Detail_Fragment;
import com.sts.asopao.Fragment.VideoMusicalFragment;
import com.sts.asopao.Preference.PrefConst;
import com.sts.asopao.Preference.Preference;
import com.sts.asopao.R;
import com.sts.asopao.Utils.BitmapUtils;
import com.sts.asopao.Utils.MultiPartRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

public class MainActivity extends BaseActivity implements NavigationView.OnNavigationItemSelectedListener{

    VideoMusicalFragment mVideoMusicalFragment;
    Top10Detail_Fragment mTop10Detail_Fragment;
    GenerosDetailFragment mGenerosDetailFragment;
    ArtistasDetailFragment mArtistasDetailFragment;

    MainAdapter mMainAdapter;

    LinearLayout lytInicio, lytGeneros, lytVideos, lytArtistas, lytTop10, lytPrivacy, lytRateApp, lytContactUs;

    NavigationView navMenu;
    View menu;
    DrawerLayout drawContainer;
    ActionBarDrawerToggle drawerToggle ;

    PagerSlidingTabStrip vpTabs;
    ViewPager vpContainer;
    public LinearLayout lytHeader;
    public SquareImageView imvAvatarUrl;

    ImageView imvMenu, imvGiveRate, imvBack, imvFarandula, imvDjs, imvVideos, imvArtistas;
    View view;
    public TextView txvAppName;
    LinearLayout lytContainer;
    FrameLayout frmContainer;

    RoundedImageView imvPhoto;
    TextView txvUserName;
    ImageView imvTakePhoto;

    LinearLayout lytLogout;

    private Uri imageCaptureUri;
    String photoPath = "";
    int indexCurrentPage = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initRateApp();

        loadLayout();
    }

    private void loadLayout() {

        drawContainer = (DrawerLayout)findViewById(R.id.drawContainer);
        navMenu = (NavigationView)findViewById(R.id.navMenu);
        drawContainer.addDrawerListener(drawerToggle);
        navMenu.setNavigationItemSelectedListener(this);
        menu = navMenu.getHeaderView(0);

        lytContactUs = (LinearLayout)menu.findViewById(R.id.lytContactUs);
        lytContactUs.setOnClickListener(this);

        lytRateApp = (LinearLayout)menu.findViewById(R.id.lytRateApp);
        lytRateApp.setOnClickListener(this);

        lytPrivacy = (LinearLayout)menu.findViewById(R.id.lytPrivacy);
        lytPrivacy.setOnClickListener(this);

        lytTop10 = (LinearLayout)menu.findViewById(R.id.lytTop10);
        lytTop10.setOnClickListener(this);

        lytArtistas = (LinearLayout)menu.findViewById(R.id.lytArtistas);
        lytArtistas.setOnClickListener(this);

        lytVideos = (LinearLayout)menu.findViewById(R.id.lytVideos);
        lytVideos.setOnClickListener(this);

        lytGeneros = (LinearLayout)menu.findViewById(R.id.lytGeneros);
        lytGeneros.setOnClickListener(this);

        lytInicio = (LinearLayout)menu.findViewById(R.id.lytInicio);
        lytInicio.setOnClickListener(this);

        lytLogout = (LinearLayout)menu.findViewById(R.id.lytLogout);
        lytLogout.setOnClickListener(this);

        imvTakePhoto = (ImageView)menu.findViewById(R.id.imvTakePhoto);
        imvTakePhoto.setOnClickListener(this);

        txvUserName = (TextView)menu.findViewById(R.id.txvUserName);
        txvUserName.setText(Commons.user.getFirstName() + " " + Commons.user.getLastName());

        imvPhoto = (RoundedImageView)menu.findViewById(R.id.imvPhoto);
        if (Commons.user.getPhotoUrl().length() > 0){
            Picasso.with(this).load(Commons.user.getPhotoUrl()).into(imvPhoto);
        }

        imvFarandula = (ImageView)findViewById(R.id.imvFarandula);
        imvFarandula.setOnClickListener(this);

        imvDjs = (ImageView)findViewById(R.id.imvDjs);
        imvDjs.setOnClickListener(this);

        imvVideos = (ImageView)findViewById(R.id.imvVideos);
        imvVideos.setOnClickListener(this);

        imvAvatarUrl = (SquareImageView)findViewById(R.id.imvAvatarUrl);

        lytContainer =  (LinearLayout) findViewById(R.id.lytContainer);
        frmContainer = (FrameLayout)findViewById(R.id.frmContainer);

        imvArtistas = (ImageView)findViewById(R.id.imvArtistas);
        imvArtistas.setOnClickListener(this);

        imvBack = (ImageView)findViewById(R.id.imvBack);
        imvBack.setOnClickListener(this);

        txvAppName = (TextView)findViewById(R.id.txvAppName);
        imvGiveRate = (ImageView)findViewById(R.id.imvGiveRate);

        vpTabs = (PagerSlidingTabStrip)findViewById(R.id.vpTabs);

        mMainAdapter =  new MainAdapter(getSupportFragmentManager(), this);
        vpContainer = (ViewPager)findViewById(R.id.vpContainer);
        vpContainer.setAdapter(mMainAdapter);

        vpTabs.setViewPager(vpContainer);
        final int pageMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 4, getResources().getDisplayMetrics());
        vpContainer.setPageMargin(pageMargin);
        vpContainer.setCurrentItem(0);
        vpContainer.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {}

            @Override
            public void onPageSelected(int position) {

                indexCurrentPage = position;

                if (position == 3 || position ==  5){
                    txvAppName.setTextColor(getResources().getColor(R.color.yellow));
                    imvGiveRate.setImageDrawable(getResources().getDrawable(R.drawable.menu_white_dot));
                }else if (position == 6){
                    txvAppName.setTextColor(getResources().getColor(R.color.yellow));
                } else {
                    txvAppName.setTextColor(getResources().getColor(R.color.yellow));
                    imvGiveRate.setImageDrawable(getResources().getDrawable(R.drawable.menu_white_dot));
                }
            }
            @Override
            public void onPageScrollStateChanged(int state) {}
        });

        lytHeader = (LinearLayout)findViewById(R.id.lytHeader);
        imvMenu = (ImageView)findViewById(R.id.imvMenu);
        imvMenu.setOnClickListener(this);
        view = (View)findViewById(R.id.view);
    }

    private void initRateApp() {
        // Set custom criteria (optional)
        RateThisApp.init(new RateThisApp.Config(3, 5));

        // Set callback (optional)
        RateThisApp.setCallback(new RateThisApp.Callback() {
            @Override
            public void onYesClicked() {

            }

            @Override
            public void onNoClicked() {

            }

            @Override
            public void onCancelClicked() {

            }
        });
    }

    public void showVideoMusicalFragment(){

        lytContainer.setVisibility(View.GONE);
        frmContainer.setVisibility(View.VISIBLE);

        imvGiveRate.setVisibility(View.GONE);
        imvBack.setVisibility(View.VISIBLE);

        txvAppName.setTextColor(getResources().getColor(R.color.yellow));
        txvAppName.setText("Video Musical");

        imvAvatarUrl.setVisibility(View.GONE);
        lytHeader.setBackgroundColor(getResources().getColor(R.color.black));

        imvVideos.setImageResource(R.drawable.play_red);

        FragmentTransaction fragmentTransaction =  getSupportFragmentManager().beginTransaction();
        mVideoMusicalFragment = new VideoMusicalFragment(this);
        fragmentTransaction.replace(R.id.frmContainer, mVideoMusicalFragment).commit();
    }

    public void showTop10Detail_Fragment(int channelId){
        lytContainer.setVisibility(View.GONE);
        frmContainer.setVisibility(View.VISIBLE);

        imvGiveRate.setVisibility(View.GONE);
        imvBack.setVisibility(View.VISIBLE);

        txvAppName.setTextColor(getResources().getColor(R.color.yellow));
        txvAppName.setText("Top 10");

        FragmentTransaction fragmentTransaction =  getSupportFragmentManager().beginTransaction();
        mTop10Detail_Fragment = new Top10Detail_Fragment(this, channelId);
    }

    public void showGenerosDetailFragment(int genreId){

        lytContainer.setVisibility(View.GONE);
        frmContainer.setVisibility(View.VISIBLE);

        imvGiveRate.setVisibility(View.GONE);
        imvBack.setVisibility(View.VISIBLE);

        FragmentTransaction fragmentTransaction =  getSupportFragmentManager().beginTransaction();
        mGenerosDetailFragment = new GenerosDetailFragment(this, genreId);
        fragmentTransaction.replace(R.id.frmContainer, mGenerosDetailFragment).commit();
    }

    public void showArtistasDetailFragment(){

        lytContainer.setVisibility(View.GONE);
        frmContainer.setVisibility(View.VISIBLE);

        imvGiveRate.setVisibility(View.GONE);
        imvBack.setVisibility(View.VISIBLE);

        FragmentTransaction fragmentTransaction =  getSupportFragmentManager().beginTransaction();
        mArtistasDetailFragment = new ArtistasDetailFragment(this);
        fragmentTransaction.replace(R.id.frmContainer, mArtistasDetailFragment).commit();
    }

    private void back(){

        lytHeader.setBackgroundColor(getResources().getColor(R.color.black));

        lytContainer.setVisibility(View.VISIBLE);
        frmContainer.setVisibility(View.GONE);

        imvGiveRate.setVisibility(View.VISIBLE);
        imvBack.setVisibility(View.GONE);

        imvVideos.setImageResource(R.drawable.play_blue);

        txvAppName.setTextColor(getResources().getColor(R.color.yellow));
        txvAppName.setText(getString(R.string.app_name));

        imvAvatarUrl.setVisibility(View.GONE);
        txvAppName.setTextColor(getResources().getColor(R.color.yellow));

        vpContainer.setCurrentItem(indexCurrentPage);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        return false;
    }

    private void takePhoto() {
        final String[] items = {"Take photo", "Choose from Gallery"};

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setItems(items, new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int item) {
                if (item == 0) {
                    takePhotoFromCamera();

                } else {
                    takePhotoFromGallery();
                }
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    private void takePhotoFromGallery() {
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType(MediaStore.Images.Media.CONTENT_TYPE);
        startActivityForResult(intent, Constants.PICK_FROM_ALBUM);
    }

    private void takePhotoFromCamera() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        String picturePath = BitmapUtils.getTempFolderPath() + "photo_temp.jpg";
        imageCaptureUri = Uri.fromFile(new File(picturePath));

        intent.putExtra(MediaStore.EXTRA_OUTPUT, imageCaptureUri);
        startActivityForResult(intent, Constants.PICK_FROM_CAMERA);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode){

            case Constants.CROP_FROM_CAMERA: {

                if (resultCode == RESULT_OK){
                    try {

                        File saveFile = BitmapUtils.getOutputMediaFile(this);

                        InputStream in = getContentResolver().openInputStream(Uri.fromFile(saveFile));
                        BitmapFactory.Options bitOpt = new BitmapFactory.Options();
                        Bitmap bitmap = BitmapFactory.decodeStream(in, null, bitOpt);
                        in.close();

                        //set The bitmap data to image View
                        imvPhoto.setImageBitmap(bitmap);
                        photoPath = saveFile.getAbsolutePath();

                        uploadPhoto();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                break;
            }
            case Constants.PICK_FROM_ALBUM:

                if (resultCode == RESULT_OK){
                    imageCaptureUri = data.getData();
                }

            case Constants.PICK_FROM_CAMERA:
            {
                try {

                    photoPath = BitmapUtils.getRealPathFromURI(this, imageCaptureUri);

                    Intent intent = new Intent("com.android.camera.action.CROP");
                    intent.setDataAndType(imageCaptureUri, "image/*");

                    intent.putExtra("crop", true);
                    intent.putExtra("scale", true);
                    intent.putExtra("outputX", Constants.PROFILE_IMAGE_SIZE);
                    intent.putExtra("outputY", Constants.PROFILE_IMAGE_SIZE);
                    intent.putExtra("aspectX", 1);
                    intent.putExtra("aspectY", 1);
                    intent.putExtra("noFaceDetection", true);
                    intent.putExtra("return-data", true);
                    intent.putExtra("output", Uri.fromFile(BitmapUtils.getOutputMediaFile(this)));

                    startActivityForResult(intent, Constants.CROP_FROM_CAMERA);
                }catch (Exception e){
                    e.printStackTrace();
                }
                break;
            }
        }
    }

    private void uploadPhoto() {
        try {

            showProgress();
            File file = new File(photoPath);

            Map<String, String> params = new HashMap<>();
            params.put(ReqConst.USER_ID, String.valueOf(Commons.user.getId()));

            String url = ReqConst.SERVER_URL + ReqConst.FUN_UPLOAD_PHOTO;

            MultiPartRequest reqMultiPart = new MultiPartRequest(url, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    closeProgress();
                    showAlertDialog(getString(R.string.photo_upload_fail));
                }
            }, new Response.Listener<String>() {

                @Override
                public void onResponse(String json) {

                    closeProgress();

                    try{
                        JSONObject response = new JSONObject(json);
                        int resultCode = response.getInt(ReqConst.RES_CODE);
                        if (resultCode == ReqConst.CODE_SUCCESS){

                            Commons.user.setPhotoUrl(response.getString(ReqConst.PHOTO_URL));
                            if (Commons.user.getPhotoUrl().length() > 0){
                                Picasso.with(MainActivity.this).load(Commons.user.getPhotoUrl()).into(imvPhoto);
                            }

                        }else {
                            showAlertDialog(getString(R.string.server_failed));
                        }
                    }catch (JSONException e){
                        showAlertDialog(getString(R.string.server_failed));
                    }

                }
            }, file, ReqConst.PARAM_FILE, params);

            reqMultiPart.setRetryPolicy(new DefaultRetryPolicy(
                    Constants.VOLLEY_TIME_OUT, 0,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            AsopaoTVApplication.getInstance().addToRequestQueue(reqMultiPart, url);

        } catch (Exception e) {

            e.printStackTrace();
            closeProgress();
            showAlertDialog(getString(R.string.photo_upload_fail));
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.imvBack:
                back();
                break;
            case R.id.imvFarandula:
                vpContainer.setCurrentItem(6);
                break;
            case R.id.imvDjs:
                vpContainer.setCurrentItem(5);
                break;
            case R.id.imvVideos:
                vpContainer.setCurrentItem(3);
                break;
            case R.id.imvArtistas:
                vpContainer.setCurrentItem(4);
                break;
            case R.id.imvMenu:
                drawContainer.openDrawer(Gravity.END);
                break;
            case R.id.imvTakePhoto:
                takePhoto();
                break;
            case R.id.lytLogout:
                Preference.getInstance().put(this, PrefConst.EMAIL, "");
                Preference.getInstance().put(this, PrefConst.PASSWORD, "");
                final Intent intent = new Intent(this, LoginActivity.class);
                startActivity(intent);
                finish();
                break;
            case R.id.lytInicio:
                drawContainer.closeDrawers();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        vpContainer.setCurrentItem(0);
                    }
                }, 300);
                break;
            case R.id.lytGeneros:
                drawContainer.closeDrawers();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        vpContainer.setCurrentItem(2);
                    }
                }, 300);
                break;
            case R.id.lytVideos:
                drawContainer.closeDrawers();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        vpContainer.setCurrentItem(3);
                    }
                }, 300);
                break;
            case R.id.lytArtistas:
                drawContainer.closeDrawers();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        vpContainer.setCurrentItem(4);
                    }
                }, 300);
                break;
            case R.id.lytTop10:
                drawContainer.closeDrawers();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        vpContainer.setCurrentItem(1);
                    }
                }, 300);
                break;
            case R.id.lytPrivacy:
                drawContainer.closeDrawers();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent intent1 = new Intent(MainActivity.this, PrivacyActivity.class);
                        startActivity(intent1);
                    }
                }, 300);
                break;
            case R.id.lytRateApp:
                drawContainer.closeDrawers();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        RateThisApp.showRateDialog(MainActivity.this, R.style.MyAlertDialogStyle2);
                    }
                }, 300);
                break;
            case R.id.lytContactUs:
                drawContainer.closeDrawers();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent intent1 = new Intent(MainActivity.this, ContactUsActivity.class);
                        startActivity(intent1);
                    }
                }, 300);
                break;
        }
    }
}
