package com.sts.asopao.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.sts.asopao.Main.MainActivity;
import com.sts.asopao.Models.ESTRENOS_Model;
import com.sts.asopao.Models.TOP_10_Model;
import com.sts.asopao.R;

import java.util.ArrayList;

/**
 * Created by STS on 12/1/2017.
 */

public class TOP_10_Adapter extends BaseAdapter {

    MainActivity activity;

    ArrayList<TOP_10_Model> allData = new ArrayList<>();

    public TOP_10_Adapter(MainActivity activity) {
        this.activity = activity;
    }

    public void refresh(ArrayList<TOP_10_Model> data){
        allData.clear();
        allData.addAll(data);
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return allData.size();
    }

    @Override
    public TOP_10_Model getItem(int position) {
        return allData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        CustomHolder holder;
        if (convertView ==  null){
            holder =  new CustomHolder();
            LayoutInflater inflater = LayoutInflater.from(activity);
            convertView = inflater.inflate(R.layout.item_top_10, parent, false);
            holder.setId(convertView);
            convertView.setTag(holder);
        }else {
            holder = (CustomHolder)convertView.getTag();
        }

        TOP_10_Model data = allData.get(position);
        holder.setData(data);

        return convertView;
    }

    private class CustomHolder{

        ImageView imvThumbnail;
        TextView txvChannelName;

        private void setId (View view){
            imvThumbnail = (ImageView)view.findViewById(R.id.imvThumbnail);
            txvChannelName = (TextView)view.findViewById(R.id.txvChannelName);
        }

        private void setData(TOP_10_Model model){
            if (model.getThumbnailUrl().length() > 0){
                Picasso.with(activity).load(model.getThumbnailUrl()).into(imvThumbnail);
            }

            txvChannelName.setText(model.getChannelName());
        }
    }
}
