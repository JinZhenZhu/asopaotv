package com.sts.asopao.Models;

import java.io.Serializable;

/**
 * Created by STS on 12/1/2017.
 */

public class FARANDULA_Model implements Serializable {
    String title = "", location = "", dateTime = "", noOfVotes = "", thumbnailUrl = "", noOfComments = "", noOfShares = "", postedTime = "";

    public String getPostedTime() {
        return postedTime;
    }

    public void setPostedTime(String postedTime) {
        this.postedTime = postedTime;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public String getNoOfComments() {
        return noOfComments;
    }

    public void setNoOfComments(String noOfComments) {
        this.noOfComments = noOfComments;
    }

    public String getNoOfShares() {
        return noOfShares;
    }

    public void setNoOfShares(String noOfShares) {
        this.noOfShares = noOfShares;
    }

    public String getNoOfVotes() {
        return noOfVotes;
    }

    public void setNoOfVotes(String noOfVotes) {
        this.noOfVotes = noOfVotes;
    }

    public String getThumbnailUrl() {
        return thumbnailUrl;
    }

    public void setThumbnailUrl(String thumbnailUrl) {
        this.thumbnailUrl = thumbnailUrl;
    }
}
