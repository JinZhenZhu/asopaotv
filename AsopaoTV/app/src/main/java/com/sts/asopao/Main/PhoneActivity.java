package com.sts.asopao.Main;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.sts.asopao.Base.BaseActivity;
import com.sts.asopao.R;

public class PhoneActivity extends BaseActivity {

    ImageView imvBack, imvNext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_phone);

        loadLayout();
        hideKeyboard();
    }

    private void loadLayout() {
        imvBack = (ImageView)findViewById(R.id.imvBack);
        imvBack.setOnClickListener(this);

        imvNext = (ImageView)findViewById(R.id.imvNext);
        imvNext.setOnClickListener(this);
    }

    private void gotoSignupActivity() {
        Intent intent = new Intent(this, SignupActivity.class);
        startActivity(intent);
        finish();
    }

    private void gotoVerificationActivity() {

        Intent intent = new Intent(this, VerificationActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onBackPressed() {
        gotoSignupActivity();
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.imvBack:
                gotoSignupActivity();
                break;
            case R.id.imvNext:
                gotoVerificationActivity();
                break;
        }
    }
}
