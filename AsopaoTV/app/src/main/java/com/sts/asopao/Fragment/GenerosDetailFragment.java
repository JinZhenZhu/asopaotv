package com.sts.asopao.Fragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.github.paolorotolo.expandableheightlistview.ExpandableHeightListView;
import com.sts.asopao.APIs.GetGenerosDataRequest;
import com.sts.asopao.Adapter.GenerosDetailAdapter;
import com.sts.asopao.Base.BaseFragment;
import com.sts.asopao.Common.Commons;
import com.sts.asopao.Common.Constants;
import com.sts.asopao.Common.ReqConst;
import com.sts.asopao.Main.MainActivity;
import com.sts.asopao.Models.GenerosDetailModel;
import com.sts.asopao.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by STS on 10/19/2017.
 */

@SuppressLint("ValidFragment")
public class GenerosDetailFragment extends BaseFragment {

    MainActivity activity;
    GenerosDetailAdapter adapter;

    ExpandableHeightListView lstGeneros;

    int genreId = 0;

    public GenerosDetailFragment(MainActivity activity, int genreId) {
        this.activity = activity;
        this.genreId = genreId;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View fragment = inflater.inflate(R.layout.fragment_generos_detail, container, false);

        adapter = new GenerosDetailAdapter(activity);
        lstGeneros = (ExpandableHeightListView)fragment.findViewById(R.id.lstGeneros);
        lstGeneros.setAdapter(adapter);
        lstGeneros.setExpanded(true);
        lstGeneros.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Commons.selectedVideoId = ((GenerosDetailModel)adapter.getItem(position)).getVideoId();
                activity.showVideoMusicalFragment();
            }
        });

        getAllData();
        return fragment;
    }

    private void getAllData() {
        final Response.Listener<String> res = new Response.Listener<String>() {
            @Override
            public void onResponse(String json) {
                activity.closeProgress();
                try{
                    JSONObject response = new JSONObject(json);
                    int resultCode = response.getInt(ReqConst.RES_CODE);
                    if (resultCode == ReqConst.CODE_SUCCESS){

                        ArrayList<GenerosDetailModel> mGenerosDetailModelList = new ArrayList<>();
                        JSONArray jsonGenerosDetailModelList = response.getJSONArray(ReqConst.VIDEOS);

                        for (int i = 0; i < jsonGenerosDetailModelList.length(); i ++){
                            GenerosDetailModel mGenerosDetailModel =  new GenerosDetailModel();
                            JSONObject jsonGenerosModel =  (JSONObject)jsonGenerosDetailModelList.get(i);

                            mGenerosDetailModel.setVideoId(jsonGenerosModel.getInt(ReqConst.VIDEO_ID));
                            mGenerosDetailModel.setTitle(jsonGenerosModel.getString(ReqConst.TITLE));
                            mGenerosDetailModel.setThumbnailUrl(jsonGenerosModel.getString(ReqConst.THUMBNAIL_URL));
                            mGenerosDetailModel.setNumberOfViews(jsonGenerosModel.getString(ReqConst.NUMBER_OF_VIEWS));
                            mGenerosDetailModel.setChannelName(jsonGenerosModel.getString(ReqConst.CHANNEL_NAME));

                            mGenerosDetailModelList.add(mGenerosDetailModel);
                        }

                        adapter.refresh(mGenerosDetailModelList);

                    }else{
                        activity.showAlertDialog(getString(R.string.server_failed));
                    }

                }catch (JSONException e){
                    activity.showAlertDialog(getString(R.string.server_failed));
                }
            }
        };
        final Response.ErrorListener error = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                activity.closeProgress();
                activity.showAlertDialog(getString(R.string.server_failed));
            }
        };

        activity.showProgress();
        GetGenerosDataRequest req = new GetGenerosDataRequest(String.valueOf(genreId), Constants.MOST_RECENT, res, error);
        req.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue request = Volley.newRequestQueue(activity);
        request.add(req);
    }

    @Override
    public void onClick(View v) {

    }
}
