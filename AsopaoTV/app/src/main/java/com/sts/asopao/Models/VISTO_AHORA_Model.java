package com.sts.asopao.Models;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by STS on 12/1/2017.
 */

public class VISTO_AHORA_Model implements Serializable {

    String title = "", duration = "", rate = "",  type = "", hd = "", videoId = "",
            createdAt = "", thumbnail = "", numberOfViews = "", noOfLikes = "",
            noOfDislikes = "", channelName = "";

    ArrayList<String> thumbnailUrlList = new ArrayList<>();

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getHd() {
        return hd;
    }

    public void setHd(String hd) {
        this.hd = hd;
    }

    public String getVideoId() {
        return videoId;
    }

    public void setVideoId(String videoId) {
        this.videoId = videoId;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getNumberOfViews() {
        return numberOfViews;
    }

    public void setNumberOfViews(String numberOfViews) {
        this.numberOfViews = numberOfViews;
    }

    public String getNoOfLikes() {
        return noOfLikes;
    }

    public void setNoOfLikes(String noOfLikes) {
        this.noOfLikes = noOfLikes;
    }

    public String getNoOfDislikes() {
        return noOfDislikes;
    }

    public void setNoOfDislikes(String noOfDislikes) {
        this.noOfDislikes = noOfDislikes;
    }

    public String getChannelName() {
        return channelName;
    }

    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }

    public ArrayList<String> getThumbnailUrlList() {
        return thumbnailUrlList;
    }

    public void setThumbnailUrlList(ArrayList<String> thumbnailUrlList) {
        this.thumbnailUrlList = thumbnailUrlList;
    }
}
