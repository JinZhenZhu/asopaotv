package com.sts.asopao.Models;

import java.io.Serializable;

/**
 * Created by STS on 12/1/2017.
 */

public class VIDEO_MUSIC_Model implements Serializable {
    int videoId = 0;
    String title = "", channelId = "", thumbnail = "", noOfVotes = "", top10 = "", thumbnailUrl = "";

    public int getVideoId() {
        return videoId;
    }

    public void setVideoId(int videoId) {
        this.videoId = videoId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getNoOfVotes() {
        return noOfVotes;
    }

    public void setNoOfVotes(String noOfVotes) {
        this.noOfVotes = noOfVotes;
    }

    public String getTop10() {
        return top10;
    }

    public void setTop10(String top10) {
        this.top10 = top10;
    }

    public String getThumbnailUrl() {
        return thumbnailUrl;
    }

    public void setThumbnailUrl(String thumbnailUrl) {
        this.thumbnailUrl = thumbnailUrl;
    }
}
