package com.sts.asopao.Main;

import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.sts.asopao.Adapter.PrivacyTermsAdapter;
import com.sts.asopao.Base.BaseActivity;
import com.sts.asopao.Models.VideoModel;
import com.sts.asopao.R;

public class PrivacyActivity extends BaseActivity {

    PrivacyTermsAdapter adapter;

    TabLayout tabLayout;
    ViewPager vpContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_privacy);

        loadLayout();
    }

    private void loadLayout() {
        tabLayout = (TabLayout)findViewById(R.id.tabLayout);
        vpContainer = (ViewPager)findViewById(R.id.vpContainer);

        adapter = new PrivacyTermsAdapter(this, getSupportFragmentManager());
        vpContainer.setAdapter(adapter);
        tabLayout.setupWithViewPager(vpContainer);
    }
}
