package com.sts.asopao.Fragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.sts.asopao.APIs.Get_MAS_VISTO_Request;
import com.sts.asopao.APIs.Get_VISTO_AHORA_Request;
import com.sts.asopao.Adapter.MAS_VISTO_Adapter;
import com.sts.asopao.Adapter.VISTO_AHORA_Adapter;
import com.sts.asopao.Base.BaseFragment;
import com.sts.asopao.Common.Commons;
import com.sts.asopao.Common.Constants;
import com.sts.asopao.Common.ReqConst;
import com.sts.asopao.Main.MainActivity;
import com.sts.asopao.Models.MAS_VISTO_Model;
import com.sts.asopao.Models.VISTO_AHORA_Model;
import com.sts.asopao.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by STS on 12/1/2017.
 */

@SuppressLint("ValidFragment")
public class VISTO_AHORA_Fragment extends BaseFragment{

    MainActivity activity;
    VISTO_AHORA_Adapter mVISTO_AHORA_Adapter;

    ListView lstMAS_VISTO_AHORA;

    public VISTO_AHORA_Fragment(MainActivity activity) {
        this.activity = activity;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View fragment = inflater.inflate(R.layout.fragment_visto_ahora, container, false);

        mVISTO_AHORA_Adapter = new VISTO_AHORA_Adapter(activity);
        lstMAS_VISTO_AHORA = (ListView)fragment.findViewById(R.id.lstMAS_VISTO_AHORA);
        lstMAS_VISTO_AHORA.setAdapter(mVISTO_AHORA_Adapter);
        lstMAS_VISTO_AHORA.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                Commons.selectedVideoId = Integer.valueOf(mVISTO_AHORA_Adapter.getItem(position).getVideoId());
                activity.showVideoMusicalFragment();
            }
        });

        return fragment;
    }

    private void getAllData() {
        final Response.Listener<String> res = new Response.Listener<String>() {
            @Override
            public void onResponse(String json) {
                activity.closeProgress();
                try{
                    JSONObject response = new JSONObject(json);
                    int resultCode = response.getInt(ReqConst.RES_CODE);
                    if (resultCode == ReqConst.CODE_SUCCESS){

                        ArrayList<VISTO_AHORA_Model> mVISTO_AHORA_Model_List =  new ArrayList<>();
                        JSONArray jsonVISTO_AHORA_Model_List = response.getJSONArray(ReqConst.VIDEOS);

                        for (int i = 0 ; i < jsonVISTO_AHORA_Model_List.length(); i++){
                            VISTO_AHORA_Model mVISTO_AHORA_Model =  new VISTO_AHORA_Model();
                            JSONObject jsonVISTO_AHORA_Model =  (JSONObject)jsonVISTO_AHORA_Model_List.get(i);

                            mVISTO_AHORA_Model.setVideoId(jsonVISTO_AHORA_Model.getString(ReqConst.VIDEO_ID));
                            mVISTO_AHORA_Model.setTitle(jsonVISTO_AHORA_Model.getString(ReqConst.TITLE));
                            mVISTO_AHORA_Model.setDuration(jsonVISTO_AHORA_Model.getString(ReqConst.DURATION));
                            mVISTO_AHORA_Model.setNumberOfViews(jsonVISTO_AHORA_Model.getString(ReqConst. NUMBER_OF_VIEWS));
                            mVISTO_AHORA_Model.setNoOfLikes(jsonVISTO_AHORA_Model.getString(ReqConst.NO_OF_LIKES));
                            mVISTO_AHORA_Model.setChannelName(jsonVISTO_AHORA_Model.getString(ReqConst.CHANNEL_NAME));

                            JSONArray jsonThumbnailUrlList = jsonVISTO_AHORA_Model.getJSONArray(ReqConst.THUMBNAIL_URLS);
                            ArrayList<String> thumbnailList = new ArrayList<>();
                            for (int j = 0; j < jsonThumbnailUrlList.length(); j ++){
                                String temp =  jsonThumbnailUrlList.get(j).toString();
                                thumbnailList.add(temp);
                            }

                            mVISTO_AHORA_Model.setThumbnailUrlList(thumbnailList);

                            mVISTO_AHORA_Model_List.add(mVISTO_AHORA_Model);
                        }

                        Commons.mVISTO_AHORA_Model_List.clear();
                        Commons.mVISTO_AHORA_Model_List.addAll(mVISTO_AHORA_Model_List);
                        mVISTO_AHORA_Adapter.refresh(mVISTO_AHORA_Model_List);

                    }else{
                        activity.showAlertDialog(getString(R.string.server_failed));
                    }

                }catch (JSONException e){
                    activity.showAlertDialog(getString(R.string.server_failed));
                }
            }
        };
        final Response.ErrorListener error = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                activity.closeProgress();
                activity.showAlertDialog(getString(R.string.server_failed));
            }
        };

        activity.showProgress();
        Get_VISTO_AHORA_Request req = new Get_VISTO_AHORA_Request(res, error);
        req.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue request = Volley.newRequestQueue(activity);
        request.add(req);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (Commons.mVISTO_AHORA_Model_List.size() > 0){
            mVISTO_AHORA_Adapter.refresh(Commons.mVISTO_AHORA_Model_List);
        }else {
            getAllData();
        }
    }
}
