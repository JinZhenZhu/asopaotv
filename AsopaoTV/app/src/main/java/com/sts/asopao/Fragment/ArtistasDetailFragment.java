package com.sts.asopao.Fragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;
import com.sts.asopao.APIs.Get_Artistas_Info_Request;
import com.sts.asopao.Adapter.ArtistasRecycleViewAdapter;
import com.sts.asopao.Base.BaseFragment;
import com.sts.asopao.Common.Commons;
import com.sts.asopao.Common.Constants;
import com.sts.asopao.Common.ReqConst;
import com.sts.asopao.Main.MainActivity;
import com.sts.asopao.Models.VideoModel;
import com.sts.asopao.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
/**
 * Created by STS on 12/1/2017.
 */

@SuppressLint("ValidFragment")
public class ArtistasDetailFragment extends BaseFragment{

    MainActivity activity;
    ArtistasRecycleViewAdapter mArtistasRecycleViewAdapter;

    TextView txvBiography, txvOtherInformation, txvArtistName, txvChannelName;
    ImageView imvAvatarUrl;
    RecyclerView rvArtistasList;

    public ArtistasDetailFragment(MainActivity activity) {
        this.activity = activity;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View fragment = inflater.inflate(R.layout.fragment_artistas_detail, container, false);

        txvBiography = (TextView)fragment.findViewById(R.id.txvBiography);
        txvOtherInformation = (TextView)fragment.findViewById(R.id.txvOtherInformation);
        txvArtistName = (TextView)fragment.findViewById(R.id.txvArtistName);
        txvChannelName = (TextView)fragment.findViewById(R.id.txvChannelName);

        imvAvatarUrl = (ImageView)fragment.findViewById(R.id.imvAvatarUrl);

        activity.lytHeader.setBackgroundColor(getResources().getColor(R.color.colorTransparentBlack));
        activity.txvAppName.setTextColor(getResources().getColor(R.color.white));

        mArtistasRecycleViewAdapter = new ArtistasRecycleViewAdapter(activity);
        rvArtistasList = (RecyclerView)fragment.findViewById(R.id.rvArtistasList);
        LinearLayoutManager horizontalLayoutManagaer = new LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false);
        rvArtistasList.setLayoutManager(horizontalLayoutManagaer);
        rvArtistasList.setAdapter(mArtistasRecycleViewAdapter);
        mArtistasRecycleViewAdapter.setClickListener(new ArtistasRecycleViewAdapter.ItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Commons.selectedVideoId =  mArtistasRecycleViewAdapter.getItem(position).getVideoId();
                activity.showVideoMusicalFragment();
            }
        });

        getAllData();

        return fragment;
    }

    private void getAllData() {
        final Response.Listener<String> res = new Response.Listener<String>() {
            @Override
            public void onResponse(String json) {
                activity.closeProgress();
                try{
                    JSONObject response = new JSONObject(json);
                    int resultCode = response.getInt(ReqConst.RES_CODE);

                    if (resultCode == ReqConst.CODE_SUCCESS){

                        JSONObject jsonArtist = response.getJSONObject("artist");
                        txvBiography.setText(jsonArtist.getString("biography"));
                        txvOtherInformation.setText(jsonArtist.getString("otherInformation"));
                        txvArtistName.setText(jsonArtist.getString("artistName"));
                        txvChannelName.setText(jsonArtist.getString("channelName"));

                        String avatarUrl = (jsonArtist.getString("avatarUrl"));
                        if (avatarUrl.length() > 0){
                            Picasso.with(activity).load(avatarUrl).into(activity.imvAvatarUrl);
                            activity.imvAvatarUrl.setVisibility(View.VISIBLE);
                        }

                        ArrayList<VideoModel> mVideoModelList = new ArrayList<>();
                        JSONArray jsonVideoModelList = response.getJSONArray("videos");

                        for (int i = 0; i < jsonVideoModelList.length(); i ++){

                            VideoModel mVideoModel = new VideoModel();
                            JSONObject jsonVideoModel = (JSONObject)jsonVideoModelList.get(i);

                            mVideoModel.setVideoId(jsonVideoModel.getInt(ReqConst.VIDEO_ID));
                            mVideoModel.setTitle(jsonVideoModel.getString(ReqConst.TITLE));

                            JSONArray jsonThumbnailUrlList = jsonVideoModel.getJSONArray(ReqConst.THUMBNAIL_URLS);
                            String thumbnailUrl =  jsonThumbnailUrlList.get(0).toString();
                            mVideoModel.setThumbnailUrl(thumbnailUrl);

                            mVideoModelList.add(mVideoModel);
                        }

                        mArtistasRecycleViewAdapter.refresh(mVideoModelList);

                    }else{
                        activity.showAlertDialog(getString(R.string.server_failed));
                    }

                }catch (JSONException e){
                    activity.showAlertDialog(getString(R.string.server_failed));
                }
            }
        };
        final Response.ErrorListener error = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                activity.closeProgress();
                activity.showAlertDialog(getString(R.string.server_failed));
            }
        };

        activity.showProgress();
        Get_Artistas_Info_Request req = new Get_Artistas_Info_Request(Commons.artistId, res, error);
        req.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue request = Volley.newRequestQueue(activity);
        request.add(req);
    }
}
