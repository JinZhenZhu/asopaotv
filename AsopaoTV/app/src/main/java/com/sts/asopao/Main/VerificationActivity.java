package com.sts.asopao.Main;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import com.sts.asopao.Base.BaseActivity;
import com.sts.asopao.R;

public class VerificationActivity extends BaseActivity {

    ImageView imvBack;
    EditText edtVerifyCode1, edtVerifyCode2, edtVerifyCode3, edtVerifyCode4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verification);

        loadLayout();
        hideKeyboard();
    }

    private void loadLayout() {

        edtVerifyCode1 = (EditText)findViewById(R.id.edtVerifyCode1);
        edtVerifyCode2 = (EditText)findViewById(R.id.edtVerifyCode2);
        edtVerifyCode3 = (EditText)findViewById(R.id.edtVerifyCode3);
        edtVerifyCode4 = (EditText)findViewById(R.id.edtVerifyCode4);

        imvBack = (ImageView)findViewById(R.id.imvBack);
        imvBack.setOnClickListener(this);
    }

    private void gotoPhoneActivity() {
        Intent intent = new Intent(this, PhoneActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onBackPressed() {
        gotoPhoneActivity();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.imvBack:
                gotoPhoneActivity();
                break;
        }
    }
}
