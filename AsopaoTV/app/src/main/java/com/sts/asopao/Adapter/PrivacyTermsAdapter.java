package com.sts.asopao.Adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.sts.asopao.Fragment.PrivacyFragment;
import com.sts.asopao.Fragment.TermsFragment;
import com.sts.asopao.Main.PrivacyActivity;

/**
 * Created by STS on 2/5/2018.
 */

public class PrivacyTermsAdapter extends FragmentStatePagerAdapter {

    PrivacyActivity activity;

    public PrivacyTermsAdapter(PrivacyActivity context, FragmentManager fm ) {

        super(fm);
        this.activity = context;
    }

    @Override
    public int getItemPosition(Object object) {
        return super.getItemPosition(object);
    }

    @Override
    public Fragment getItem(int position) {
        Fragment frag=null;
        switch (position){

            case 0:
                frag=new PrivacyFragment(activity);
                break;

            case 1:
                frag = new TermsFragment(activity);
                break;
        }
        return frag;
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        String title="";
        switch (position){
            case 0:
                title="Privacy & Policy";
                break;
            case 1:
                title="Terms";
                break;
        }
        return title;
    }
}
