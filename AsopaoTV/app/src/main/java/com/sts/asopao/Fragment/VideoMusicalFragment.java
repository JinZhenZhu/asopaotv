package com.sts.asopao.Fragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;
import com.sts.asopao.APIs.Get_Video_Music_Request;
import com.sts.asopao.Adapter.VIDEO_MUSIC_Adapter;
import com.sts.asopao.Base.BaseFragment;
import com.sts.asopao.Common.Commons;
import com.sts.asopao.Common.Constants;
import com.sts.asopao.Common.ReqConst;
import com.sts.asopao.CustomView.CustomVideoView.JZMediaManager;
import com.sts.asopao.CustomView.CustomVideoView.JZVideoPlayerStandard;
import com.sts.asopao.CustomView.CustomVideoView.MyJZVideoPlayerStandard;
import com.sts.asopao.Main.MainActivity;
import com.sts.asopao.Models.VIDEO_MUSIC_Model;
import com.sts.asopao.Models.VideoPlayModel;
import com.sts.asopao.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by STS on 12/1/2017.
 */

@SuppressLint("ValidFragment")
public class VideoMusicalFragment extends BaseFragment{

    MainActivity activity;

    VIDEO_MUSIC_Adapter mVIDEO_MUSIC_Adapter;

    RelativeLayout rytVote;
    View vwVote;
    MyJZVideoPlayerStandard videoPlayer;
    ListView lstPlayer;
    TextView txvNoOfLikes, txvNumberOfViews, txvVotosCount, txvTitle;
    ImageView imvThumbnail;

    int videoPosition = 0;

    public VideoMusicalFragment(MainActivity activity) {
        this.activity = activity;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View fragment = inflater.inflate(R.layout.fragment_video_musical, container, false);

        txvNoOfLikes = (TextView)fragment.findViewById(R.id.txvNoOfLikes);
        txvNumberOfViews = (TextView)fragment.findViewById(R.id.txvNumberOfViews);
        txvVotosCount = (TextView)fragment.findViewById(R.id.txvVotosCount);
        txvTitle = (TextView)fragment.findViewById(R.id.txvTitle);
        imvThumbnail = (ImageView)fragment.findViewById(R.id.imvThumbnail);

        rytVote = (RelativeLayout)fragment.findViewById(R.id.rytVote);
        rytVote.setOnClickListener(this);

        videoPlayer = (MyJZVideoPlayerStandard)fragment.findViewById(R.id.videoPlayer);
        videoPlayer.imvReload.setOnClickListener(this);
        videoPlayer.imvNext.setOnClickListener(this);

        vwVote = (View)fragment.findViewById(R.id.vwVote);

        mVIDEO_MUSIC_Adapter =  new VIDEO_MUSIC_Adapter(activity);
        lstPlayer = (ListView)fragment.findViewById(R.id.lstPlayer);
        lstPlayer.setAdapter(mVIDEO_MUSIC_Adapter);
        lstPlayer.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Commons.selectedVideoId = mVIDEO_MUSIC_Adapter.getItem(position).getVideoId();
                getAllData();
            }
        });


        getAllData();

        return fragment;
    }

    private void getAllData() {
        final Response.Listener<String> res = new Response.Listener<String>() {
            @Override
            public void onResponse(String json) {
                activity.closeProgress();
                try{
                    JSONObject response = new JSONObject(json);
                    int resultCode = response.getInt(ReqConst.RES_CODE);
                    if (resultCode == ReqConst.CODE_SUCCESS){

                        VideoPlayModel mVideoPlayerModel = new VideoPlayModel();
                        JSONObject jsonVideoPlayerModel = response.getJSONObject(ReqConst.VIDEO);

                        mVideoPlayerModel.setVideoId(jsonVideoPlayerModel.getInt(ReqConst.VIDEO_ID));
                        mVideoPlayerModel.setTitle(jsonVideoPlayerModel.getString(ReqConst.TITLE));
                        mVideoPlayerModel.setNumberOfViews(jsonVideoPlayerModel.getString(ReqConst.NUMBER_OF_VIEWS));
                        mVideoPlayerModel.setVideoUrl(jsonVideoPlayerModel.getString(ReqConst.VIDEO_URL));
                        mVideoPlayerModel.setThumbnailUrl(jsonVideoPlayerModel.getString(ReqConst.THUMBNAIL_URL));
                        mVideoPlayerModel.setNoOfLikes(jsonVideoPlayerModel.getString(ReqConst.NO_OF_LIKES));
                        mVideoPlayerModel.setNoOfDislikes(jsonVideoPlayerModel.getString(ReqConst.NO_OF_DISLIKES));
                        mVideoPlayerModel.setNoOfVotes(jsonVideoPlayerModel.getString(ReqConst.NO_OF_VOTES));

                        loadVideo(mVideoPlayerModel);

                        ArrayList<VIDEO_MUSIC_Model> mVIDEO_MUSIC_Model_List =  new ArrayList<>();
                        JSONArray jsonVIDEO_MUSIC_Model_List = response.getJSONArray(ReqConst.TOP_TEM_RELATED_VIDEOS);

                        int votoCount = 0;
                        for (int i =  0; i < jsonVIDEO_MUSIC_Model_List.length(); i++){
                            VIDEO_MUSIC_Model mVIDEO_MUSIC_Model =  new VIDEO_MUSIC_Model();
                            JSONObject jsonVIDEO_MUSIC_Model = (JSONObject) jsonVIDEO_MUSIC_Model_List.get(i);

                            mVIDEO_MUSIC_Model.setVideoId(jsonVIDEO_MUSIC_Model.getInt(ReqConst.VIDEO_ID));
                            mVIDEO_MUSIC_Model.setTitle(jsonVIDEO_MUSIC_Model.getString(ReqConst.TITLE));
                            mVIDEO_MUSIC_Model.setThumbnailUrl(jsonVIDEO_MUSIC_Model.getString(ReqConst.THUMBNAIL_URL));
                            mVIDEO_MUSIC_Model.setNoOfVotes(jsonVIDEO_MUSIC_Model.getString(ReqConst.NO_OF_VOTES));

                            votoCount = votoCount + Integer.valueOf(mVIDEO_MUSIC_Model.getNoOfVotes());

                            mVIDEO_MUSIC_Model_List.add(mVIDEO_MUSIC_Model);
                        }

                        mVIDEO_MUSIC_Adapter.refresh(mVIDEO_MUSIC_Model_List);
                    }else if (resultCode == ReqConst.CODE_NULL_VIDEO){
                        activity.showToast("No exist!");
                    }

                }catch (JSONException e){
                    activity.showAlertDialog(getString(R.string.server_failed));
                }
            }
        };
        final Response.ErrorListener error = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                activity.closeProgress();
                activity.showAlertDialog(getString(R.string.server_failed));
            }
        };

        activity.showProgress();
        Get_Video_Music_Request req = new Get_Video_Music_Request(String.valueOf(Commons.selectedVideoId), res, error);
        req.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue request = Volley.newRequestQueue(activity);
        request.add(req);
    }

    private void loadVideo(VideoPlayModel model) {

        txvNoOfLikes.setText(model.getNoOfLikes());
        txvNumberOfViews.setText("+" + model.getNumberOfViews());
        txvVotosCount.setText("+" + model.getNoOfVotes());
        txvTitle.setText(model.getTitle());

        if (Commons.artiastasPhotoUrl.length() > 0){
            Picasso.with(activity).load(Commons.artiastasPhotoUrl).into(imvThumbnail);
        }

        videoPlayer.setUp(model.getVideoUrl(),
                JZVideoPlayerStandard.SCREEN_LAYOUT_NORMAL, model.getTitle());
        Picasso.with(activity)
                .load(model.getThumbnailUrl())
                .into(videoPlayer.thumbImageView);

        videoPlayer.startVideo();
    }

    private void giveVote() {
        vwVote.setVisibility(View.VISIBLE);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.rytVote:
                giveVote();
                break;
            case R.id.imvReload:
                if (videoPlayer.currentState == videoPlayer.CURRENT_STATE_AUTO_COMPLETE) return;
                videoPlayer.progressBar.setProgress(videoPlayer.progressBar.getProgress() - 10);
                JZMediaManager.instance().mediaPlayer.seekTo((videoPlayer.progressBar.getProgress() * videoPlayer.getDuration())/100 - 10);
                break;
            case R.id.imvNext:
                if (videoPosition < mVIDEO_MUSIC_Adapter.getSize()){
                    Commons.selectedVideoId =mVIDEO_MUSIC_Adapter.getItem(videoPosition).getVideoId();
                    getAllData();
                    videoPosition++;
                }
                break;
        }
    }
}
