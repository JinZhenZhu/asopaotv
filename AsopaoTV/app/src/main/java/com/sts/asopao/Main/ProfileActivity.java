package com.sts.asopao.Main;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.makeramen.roundedimageview.RoundedImageView;
import com.squareup.picasso.Picasso;
import com.sts.asopao.AsopaoTVApplication;
import com.sts.asopao.Base.BaseActivity;
import com.sts.asopao.Common.Commons;
import com.sts.asopao.Common.Constants;
import com.sts.asopao.Common.ReqConst;
import com.sts.asopao.Preference.PrefConst;
import com.sts.asopao.Preference.Preference;
import com.sts.asopao.R;
import com.sts.asopao.Utils.BitmapUtils;
import com.sts.asopao.Utils.MultiPartRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

public class ProfileActivity extends BaseActivity {

    Button btnLogout;
    LinearLayout lytBack;
    RoundedImageView imvPhoto;
    TextView txvUserName, txvEmail;

    private Uri imageCaptureUri;
    String photoPath = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        loadLayout();
    }

    private void loadLayout() {

        btnLogout = (Button)findViewById(R.id.btnLogout);
        btnLogout.setOnClickListener(this);

        lytBack = (LinearLayout)findViewById(R.id.lytBack);
        lytBack.setOnClickListener(this);

        imvPhoto = (RoundedImageView)findViewById(R.id.imvPhoto);
        imvPhoto.setOnClickListener(this);
        if (Commons.user.getPhotoUrl().length() > 0){
            Picasso.with(this).load(Commons.user.getPhotoUrl()).into(imvPhoto);
        }

        txvUserName = (TextView)findViewById(R.id.txvUserName);
        txvUserName.setText(Commons.user.getFirstName() + " " + Commons.user.getLastName());

        txvEmail = (TextView)findViewById(R.id.txvEmail);
        txvEmail.setText(Commons.user.getEmail());
    }

    private void logout() {
        Preference.getInstance().put(this, PrefConst.EMAIL, "");
        Preference.getInstance().put(this, PrefConst.PASSWORD, "");

        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    private void takePhoto() {
        final String[] items = {"Take photo", "Choose from Gallery"};

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setItems(items, new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int item) {
                if (item == 0) {
                    takePhotoFromCamera();

                } else {
                    takePhotoFromGallery();
                }
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    private void takePhotoFromGallery() {
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType(MediaStore.Images.Media.CONTENT_TYPE);
        startActivityForResult(intent, Constants.PICK_FROM_ALBUM);
    }

    private void takePhotoFromCamera() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        String picturePath = BitmapUtils.getTempFolderPath() + "photo_temp.jpg";
        imageCaptureUri = Uri.fromFile(new File(picturePath));

        intent.putExtra(MediaStore.EXTRA_OUTPUT, imageCaptureUri);
        startActivityForResult(intent, Constants.PICK_FROM_CAMERA);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode){

            case Constants.CROP_FROM_CAMERA: {

                if (resultCode == RESULT_OK){
                    try {

                        File saveFile = BitmapUtils.getOutputMediaFile(this);

                        InputStream in = getContentResolver().openInputStream(Uri.fromFile(saveFile));
                        BitmapFactory.Options bitOpt = new BitmapFactory.Options();
                        Bitmap bitmap = BitmapFactory.decodeStream(in, null, bitOpt);
                        in.close();

                        //set The bitmap data to image View
                        imvPhoto.setImageBitmap(bitmap);
                        photoPath = saveFile.getAbsolutePath();

                        uploadPhoto();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                break;
            }
            case Constants.PICK_FROM_ALBUM:

                if (resultCode == RESULT_OK){
                    imageCaptureUri = data.getData();
                }

            case Constants.PICK_FROM_CAMERA:
            {
                try {

                    photoPath = BitmapUtils.getRealPathFromURI(this, imageCaptureUri);

                    Intent intent = new Intent("com.android.camera.action.CROP");
                    intent.setDataAndType(imageCaptureUri, "image/*");

                    intent.putExtra("crop", true);
                    intent.putExtra("scale", true);
                    intent.putExtra("outputX", Constants.PROFILE_IMAGE_SIZE);
                    intent.putExtra("outputY", Constants.PROFILE_IMAGE_SIZE);
                    intent.putExtra("aspectX", 1);
                    intent.putExtra("aspectY", 1);
                    intent.putExtra("noFaceDetection", true);
                    intent.putExtra("return-data", true);
                    intent.putExtra("output", Uri.fromFile(BitmapUtils.getOutputMediaFile(this)));

                    startActivityForResult(intent, Constants.CROP_FROM_CAMERA);
                }catch (Exception e){
                    e.printStackTrace();
                }
                break;
            }
        }
    }

    private void uploadPhoto() {
        try {

            showProgress();
            File file = new File(photoPath);

            Map<String, String> params = new HashMap<>();
            params.put(ReqConst.USER_ID, String.valueOf(Commons.user.getId()));

            String url = ReqConst.SERVER_URL + ReqConst.FUN_UPLOAD_PHOTO;

            MultiPartRequest reqMultiPart = new MultiPartRequest(url, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    closeProgress();
                    showAlertDialog(getString(R.string.photo_upload_fail));
                }
            }, new Response.Listener<String>() {

                @Override
                public void onResponse(String json) {

                    closeProgress();

                    try{
                        JSONObject response = new JSONObject(json);
                        int resultCode = response.getInt(ReqConst.RES_CODE);
                        if (resultCode == ReqConst.CODE_SUCCESS){

                            Commons.user.setPhotoUrl(response.getString(ReqConst.PHOTO_URL));


                        }else {
                            showAlertDialog(getString(R.string.server_failed));
                        }
                    }catch (JSONException e){
                        showAlertDialog(getString(R.string.server_failed));
                    }

                }
            }, file, ReqConst.PARAM_FILE, params);

            reqMultiPart.setRetryPolicy(new DefaultRetryPolicy(
                    Constants.VOLLEY_TIME_OUT, 0,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            AsopaoTVApplication.getInstance().addToRequestQueue(reqMultiPart, url);

        } catch (Exception e) {

            e.printStackTrace();
            closeProgress();
            showAlertDialog(getString(R.string.photo_upload_fail));
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnLogout:
                logout();
                break;
            case R.id.lytBack:
                Intent intent = new Intent(this, MainActivity.class);
                startActivity(intent);
                finish();
                break;
            case R.id.imvPhoto:
                takePhoto();
                break;
        }
    }
}
