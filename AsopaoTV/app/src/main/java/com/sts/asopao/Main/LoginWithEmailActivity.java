package com.sts.asopao.Main;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.sts.asopao.APIs.LoginRequest;
import com.sts.asopao.Base.BaseActivity;
import com.sts.asopao.Common.Commons;
import com.sts.asopao.Common.Constants;
import com.sts.asopao.Common.ReqConst;
import com.sts.asopao.Models.UserModel;
import com.sts.asopao.Preference.PrefConst;
import com.sts.asopao.Preference.Preference;
import com.sts.asopao.R;

import org.json.JSONException;
import org.json.JSONObject;

public class LoginWithEmailActivity extends BaseActivity{

    ImageView imvBack;
    TextView txvSignup;
    Button btnSignin;
    EditText edtEmail, edtPwd;

    String email, pwd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_with_email);

        loadLayout();
        hideKeyboard();
    }

    private void loadLayout() {

        btnSignin = (Button)findViewById(R.id.btnSignin);
        btnSignin.setOnClickListener(this);

        imvBack = (ImageView)findViewById(R.id.imvBack);
        imvBack.setOnClickListener(this);

        txvSignup = (TextView)findViewById(R.id.txvSignup);
        txvSignup.setOnClickListener(this);

        edtEmail = (EditText)findViewById(R.id.edtEmail);
        edtPwd = (EditText)findViewById(R.id.edtPwd);
    }

    private void gotoLoginActivity() {

        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        gotoLoginActivity();
    }

    private void gotoSignupActivity() {
        Intent intent = new Intent(this, SignupActivity.class);
        startActivity(intent);
        finish();
    }

    private void gotoMainActivity() {

        Preference.getInstance().put(this, PrefConst.EMAIL, email);
        Preference.getInstance().put(this, PrefConst.PASSWORD, pwd);
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    private boolean valid() {
        email = edtEmail.getText().toString().trim();
        pwd = edtPwd.getText().toString().trim();

        if (email.length() == 0){
            showAlertDialog("Please input email");
            return false;
        }else if (pwd.length() == 0){
            showAlertDialog("Please input password");
            return false;
        }

        return true;
    }

    private void processLogin() {

        final Response.Listener<String> res = new Response.Listener<String>() {
            @Override
            public void onResponse(String json) {
                closeProgress();
                try{
                    JSONObject response = new JSONObject(json);
                    int resultCode = response.getInt(ReqConst.RES_CODE);
                    if (resultCode == ReqConst.CODE_SUCCESS){

                        JSONObject jsonUser = response.getJSONObject(ReqConst.USER_MODEL);
                        UserModel userModel = new UserModel();
                        userModel.setId(jsonUser.getInt(ReqConst.ID));
                        userModel.setFirstName(jsonUser.getString(ReqConst.FIRST_NAME));
                        userModel.setLastName(jsonUser.getString(ReqConst.LAST_NAME));
                        userModel.setEmail(jsonUser.getString(ReqConst.EMAIL));
                        userModel.setPhotoUrl(jsonUser.getString(ReqConst.PHOTO_URL));

                        Commons.user = userModel;
                        gotoMainActivity();

                    }else if (resultCode == ReqConst.CODE_UNREGISTERED_USER){
                        showAlertDialog("Unregistered User");
                    }else if (resultCode == ReqConst.CODE_EMAIL_PWD_ERROR){
                        showAlertDialog("Email or Password Error!");
                    }
                }catch (JSONException e){
                    showAlertDialog(getString(R.string.server_failed));
                }
            }
        };
        final Response.ErrorListener error = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                closeProgress();
                showAlertDialog(getString(R.string.server_failed));
            }
        };

        showProgress();
        LoginRequest req = new LoginRequest(email, pwd, res, error);
        req.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue request = Volley.newRequestQueue(this);
        request.add(req);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.imvBack:
                gotoLoginActivity();
                break;
            case R.id.txvSignup:
                gotoSignupActivity();
                break;
            case R.id.btnSignin:
                if (valid())
                    processLogin();
                break;
        }
    }
}
