package com.sts.asopao.Fragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sts.asopao.Base.BaseFragment;
import com.sts.asopao.Common.PrivacyTermsClass;
import com.sts.asopao.Main.PrivacyActivity;
import com.sts.asopao.R;

/**
 * Created by STS on 12/1/2017.
 */

@SuppressLint("ValidFragment")
public class TermsFragment extends BaseFragment{

    PrivacyActivity activity;

    TextView txvTerms;

    public TermsFragment(PrivacyActivity activity) {
        this.activity = activity;
    }

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View fragment = inflater.inflate(R.layout.fragment_terms, container, false);

        txvTerms = (TextView)fragment.findViewById(R.id.txvTerms);
        txvTerms.setText(PrivacyTermsClass.terms);

        return fragment;
    }
}
