package com.sts.asopao.Main;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Build;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.facebook.CallbackManager;
import com.facebook.FacebookActivity;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.sts.asopao.APIs.LoginRequest;
import com.sts.asopao.APIs.LoginWithSocialRequest;
import com.sts.asopao.Base.BaseActivity;
import com.sts.asopao.Common.Commons;
import com.sts.asopao.Common.Constants;
import com.sts.asopao.Common.ReqConst;
import com.sts.asopao.Models.UserModel;
import com.sts.asopao.Preference.PrefConst;
import com.sts.asopao.Preference.Preference;
import com.sts.asopao.R;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.Twitter;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterConfig;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterAuthClient;
import com.twitter.sdk.android.core.identity.TwitterLoginButton;
import com.twitter.sdk.android.core.services.AccountService;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

public class LoginActivity extends BaseActivity implements GoogleApiClient.OnConnectionFailedListener {

    ImageView imvLoginWithEmail, imvLoginWithGoogle, imvLoginWithFacebook, imvLoginWithTwitter;
    GoogleApiClient mGoogleApiClient;
    CallbackManager callbackManager;
    private TwitterAuthClient twitterAuthClient;
    TwitterLoginButton btnTwitter;

    String firstName = "", lastName = "", email = "", password = "", photoUrl = "";
    String[] PERMISSIONS = {android.Manifest.permission.READ_EXTERNAL_STORAGE,  android.Manifest.permission.WRITE_EXTERNAL_STORAGE, android.Manifest.permission.READ_SMS, android.Manifest.permission.CAMERA, android.Manifest.permission.CALL_PHONE, android.Manifest.permission.ACCESS_COARSE_LOCATION, android.Manifest.permission.ACCESS_FINE_LOCATION};

    private final String FACEBOOK_LOGIN = "facebook";
    private final String TWITTER_LOGIN = "twitter";
    String loginKey = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        checkAllPermission();

        loadLayout();

        initSocialLoginRequest();
    }

    private void loadLayout() {

        imvLoginWithTwitter = (ImageView)findViewById(R.id.imvLoginWithTwitter);
        imvLoginWithTwitter.setOnClickListener(this);

        imvLoginWithFacebook = (ImageView)findViewById(R.id.imvLoginWithFacebook);
        imvLoginWithFacebook.setOnClickListener(this);

        imvLoginWithEmail = (ImageView)findViewById(R.id.imvLoginWithEmail);
        imvLoginWithEmail.setOnClickListener(this);

        imvLoginWithGoogle = (ImageView)findViewById(R.id.imvLoginWithGoogle);
        imvLoginWithGoogle.setOnClickListener(this);

        email = Preference.getInstance().getValue(this, PrefConst.EMAIL, "");
        password = Preference.getInstance().getValue(this, PrefConst.PASSWORD, "");
        if (email.length() > 0){
            autoLogin();
        }

        btnTwitter = (TwitterLoginButton)findViewById(R.id.btnTwitter);
        /*btnTwitter.setCallback(new Callback<TwitterSession>() {
            @Override
            public void success(Result<TwitterSession> result) {
                handleTwitterSession(result.data);
            }

            @Override
            public void failure(TwitterException exception) {
                Toast.makeText(LoginActivity.this, "failure", Toast.LENGTH_SHORT).show();
            }
        });*/
    }

    private void handleTwitterSession(TwitterSession data) {

    }

    private void gotoLoginWithEmailActivity() {
        Intent intent = new Intent(this, LoginWithEmailActivity.class);
        startActivity(intent);
        finish();
    }

    private void initSocialLoginRequest() {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        /// initialize facebook login
        FacebookSdk.sdkInitialize(this.getApplicationContext());

        callbackManager = CallbackManager.Factory.create();

        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "com.sts.asopao",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) { }


        // initialize Twitter login
        TwitterAuthConfig authConfig =  new TwitterAuthConfig(
                getString(R.string.twitter_consumer_key),
                getString(R.string.twitter_consumer_secret));
        TwitterConfig twitterConfig = new TwitterConfig.Builder(this)
                .twitterAuthConfig(authConfig)
                .build();
        Twitter.initialize(twitterConfig);

        twitterAuthClient = new TwitterAuthClient();
    }

    private void loginWithGoogle() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, ReqConst.CODE_GOOGLE_LOGIN);
    }

    private void handleGoogleSingInResult(GoogleSignInResult result) {
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();
            firstName = acct.getDisplayName().split(" ")[0];
            try{
                lastName = acct.getDisplayName().split(" ")[1];
            }catch (Exception e){}

            email = acct.getEmail();
            password = acct.getId();

            registerUserInfo();

        } else {
            // Signed out, show unauthenticated UI.
        }
    }

    private void registerUserInfo() {

        final Response.Listener<String> res = new Response.Listener<String>() {
            @Override
            public void onResponse(String json) {
                closeProgress();
                try{
                    JSONObject response = new JSONObject(json);
                    int resultCode = response.getInt(ReqConst.RES_CODE);
                    if (resultCode == ReqConst.CODE_SUCCESS){

                        JSONObject jsonUser = response.getJSONObject(ReqConst.USER_MODEL);
                        UserModel userModel = new UserModel();
                        userModel.setId(jsonUser.getInt(ReqConst.ID));
                        userModel.setFirstName(jsonUser.getString(ReqConst.FIRST_NAME));
                        userModel.setLastName(jsonUser.getString(ReqConst.LAST_NAME));
                        userModel.setEmail(jsonUser.getString(ReqConst.EMAIL));
                        userModel.setPhotoUrl(jsonUser.getString(ReqConst.PHOTO_URL));

                        Commons.user = userModel;

                        gotoMainActivity();
                    }else {
                        showAlertDialog(getString(R.string.server_failed));
                    }
                }catch (JSONException e){
                    showAlertDialog(getString(R.string.server_failed));
                }
            }
        };
        final Response.ErrorListener error = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                closeProgress();
                showAlertDialog(getString(R.string.server_failed));
            }
        };

        showProgress();
        LoginWithSocialRequest req = new LoginWithSocialRequest(firstName, lastName, email, password, photoUrl, res, error);
        req.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue request = Volley.newRequestQueue(this);
        request.add(req);
    }

    private void gotoMainActivity() {

        Preference.getInstance().put(this, PrefConst.EMAIL, email);
        Preference.getInstance().put(this, PrefConst.PASSWORD, password);

        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == ReqConst.CODE_GOOGLE_LOGIN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleGoogleSingInResult(result);
        }

        if (loginKey.equals(FACEBOOK_LOGIN)) callbackManager.onActivityResult(requestCode, resultCode, data);

        if (loginKey.equals(TWITTER_LOGIN)) btnTwitter.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.d("Google Connect Fail", "Connection Failed");
    }

    private void loginWithFacebook() {
        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "email"));
        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        GraphRequest request = GraphRequest.newMeRequest(
                                loginResult.getAccessToken(),
                                new GraphRequest.GraphJSONObjectCallback() {
                                    @Override
                                    public void onCompleted(JSONObject object, GraphResponse response) {
                                        Log.v("LoginActivity", response.toString());

                                        try {
                                            // Application code
                                            email = object.getString("email");

                                            String fullName = object.getString("name");
                                            firstName = fullName.split(" ")[0];
                                            try{
                                                lastName = fullName.split(" ")[1];
                                            }catch (Exception e){}

                                            password = object.getString("id");
                                            photoUrl = "https://graph.facebook.com/" + password + "/picture?type=large";

                                            registerUserInfo();

                                        }catch (Exception e){}
                                    }
                                });
                        Bundle parameters = new Bundle();
                        parameters.putString("fields", "id,name,email,gender,birthday");
                        request.setParameters(parameters);
                        request.executeAsync();
                    }

                    @Override
                    public void onCancel() {
                        Toast.makeText(LoginActivity.this, "Login Cancel", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        Toast.makeText(LoginActivity.this, exception.getMessage(), Toast.LENGTH_LONG).show();
                    }
                });
    }

    private void autoLogin() {
        final Response.Listener<String> res = new Response.Listener<String>() {
            @Override
            public void onResponse(String json) {
                closeProgress();
                try{
                    JSONObject response = new JSONObject(json);
                    int resultCode = response.getInt(ReqConst.RES_CODE);
                    if (resultCode == ReqConst.CODE_SUCCESS){

                        JSONObject jsonUser = response.getJSONObject(ReqConst.USER_MODEL);
                        UserModel userModel = new UserModel();
                        userModel.setId(jsonUser.getInt(ReqConst.ID));
                        userModel.setFirstName(jsonUser.getString(ReqConst.FIRST_NAME));
                        userModel.setLastName(jsonUser.getString(ReqConst.LAST_NAME));
                        userModel.setEmail(jsonUser.getString(ReqConst.EMAIL));
                        userModel.setPhotoUrl(jsonUser.getString(ReqConst.PHOTO_URL));

                        Commons.user = userModel;
                        gotoMainActivity();

                    }else if (resultCode == ReqConst.CODE_UNREGISTERED_USER){
                        showAlertDialog("Unregistered User");
                    }else if (resultCode == ReqConst.CODE_EMAIL_PWD_ERROR){
                        showAlertDialog("Email or Password Error!");
                    }
                }catch (JSONException e){
                    showAlertDialog(getString(R.string.server_failed));
                }
            }
        };
        final Response.ErrorListener error = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                closeProgress();
                showAlertDialog(getString(R.string.server_failed));
            }
        };

        showProgress();
        LoginRequest req = new LoginRequest(email, password, res, error);
        req.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue request = Volley.newRequestQueue(this);
        request.add(req);
    }

    private void loginWithTwitter() {

        twitterAuthClient.authorize(LoginActivity.this, new Callback<TwitterSession>() {
            @Override
            public void success(Result<TwitterSession> result) {
                final long id =  result.data.getUserId();
                firstName = result.data.getUserName();
                password = String.valueOf(id);

                TwitterSession session = result.data;
                twitterAuthClient.requestEmail(session, new Callback<String>() {
                    @Override
                    public void success(Result<String> result) {
                        email = result.data;
                        registerUserInfo();
                    }

                    @Override
                    public void failure(TwitterException exception) {
                        Log.d("Twitter===", "Failed");
                    }
                });
            }

            @Override
            public void failure(TwitterException exception) {

            }
        });
    }

    //==================== Permission========================================
    public void checkAllPermission() {

        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        if (hasPermissions(this, PERMISSIONS)){

        }else {
            ActivityCompat.requestPermissions(this, PERMISSIONS, 101);
        }
    }

    public boolean hasPermissions(Context context, String... permissions) {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {

            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == Constants.MY_PEQUEST_CODE && grantResults[0] == PackageManager.PERMISSION_GRANTED) {}
    }
    //==================================================================

    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.imvLoginWithEmail:
                gotoLoginWithEmailActivity();
                break;
            case R.id.imvLoginWithGoogle:
                loginWithGoogle();
                break;
            case R.id.imvLoginWithFacebook:
                loginKey = FACEBOOK_LOGIN;
                loginWithFacebook();
                break;
            case R.id.imvLoginWithTwitter:
                loginKey = TWITTER_LOGIN;
                loginWithTwitter();
                break;
        }
    }
}
