package com.sts.asopao.APIs;

import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;
import com.sts.asopao.Common.ReqConst;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by STS on 2/9/2017.
 */

public class Get_Video_Music_Request extends StringRequest {

    private static  final String url =  ReqConst.SERVER_URL + ReqConst.FUN_VIDEO_PLAY;
    private Map<String,String> params;

    public Get_Video_Music_Request(String videoId, Response.Listener<String> listener, Response.ErrorListener error) {

        super(Method.POST, url, listener,error);

        params = new HashMap<>();
        params.put(ReqConst.VIDEO_ID, videoId);
    }

    @Override
    public Map<String, String> getParams() {
        return params;
    }
}
