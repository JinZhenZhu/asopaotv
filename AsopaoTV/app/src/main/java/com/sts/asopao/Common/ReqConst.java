package com.sts.asopao.Common;

import java.net.PortUnreachableException;

/**
 * Created by STS on 10/27/2017.
 */

public class ReqConst {
    public static final String SERVER_URL = "http://asopao.com/AndroidApi/";

    public static final String RES_CODE = "resultCode";
    public static final int CODE_GOOGLE_LOGIN = 10;
    public static final int CODE_FACEBOOK_LOGIN = 11;
    public static final int CODE_SUCCESS = 100;
    public static final int CODE_UNREGISTERED_USER = 101;
    public static final int CODE_EMAIL_PWD_ERROR = 102;
    public static final int CODE_NULL_VIDEO = 103;

    /////// Login ///
    public static final String FUN_LOGIN = "login";
    public static final String EMAIL = "email";
    public static final String PASSWORD = "password";
    public static final String USER_MODEL = "userModel";
    public static final String ID = "id";
    public static final String FIRST_NAME = "firstName";
    public static final String LAST_NAME = "lastName";
    public static final String PHOTO_URL = "photoUrl";
    public static final String USER_ID = "userId";

    // upload photo
    public static final String FUN_UPLOAD_PHOTO = "uploadPhoto";
    public static final String PARAM_FILE = "file";

    /// Signup ///
    public static final String FUN_SIGNUP = "signup";

    //// Login with Google //
    public static final String FUN_LOGIN_WITH_SOCIAL = "loginWithSocial";

    //// FUN_GET_ESTRENOS_DATA //
    public static final String FUN_GET_ESTRENOS_DATA = "getVideos";
    public static final String VIDEOS = "videos";
    public static final String VIDEO_ID = "videoId";
    public static final String TITLE = "title";
    public static final String DURATION = "duration";
    public static final String VIDEO_NAME = "videoName";
    public static final String NUMBER_OF_VIEWS = "numberOfViews";
    public static final String RATING = "rating";
    public static final String NO_OF_LIKES = "noOfLikes";
    public static final String NO_OF_DISLIKES = "noOfDislikes";
    public static final String VIDEO_TYPE = "videoType";
    public static final String HD = "HD";
    public static final String CHANNEL_NAME = "channelName";
    public static final String THUMBNAIL_URL = "thumbnailUrl";
    public static final String KEY = "key";
    public static final String THUMBNAIL_URLS = "thumbnailUrls";
    //// Top_10
    public static final String FUN_GET_TOP_10_DATA = "getTopChannels";
    public static final String TOP_CHANNELS = "topChannels";
    public static final String CHANNEL_ID = "channelId";

    /// Player page
    public static final String FUN_GET_PLAYER_TAB_DATA = "videos";
    public static final String PAGER_NUMBER = "pageNumber";

    //// Generos
    public static final String FUN_GET_GENEROS_TAB_DATA = "getGeneros";
    public static final String GENEROS_ID = "genreId";
    public static final String NAME = "name";
    public static final String GENEROS = "generos";

    /// PlayVideo
    public static final String FUN_VIDEO_PLAY = "video/play";
    public static final String VIDEO = "video";
    public static final String VIDEO_URL = "videoUrl";
    public static final String DATE_RANGE = "dateRange";
    public static final String TOP_TEM_RELATED_VIDEOS = "topTenRelatedVideos";
    public static final String NO_OF_VOTES = "noOfVotes";
    public static final String NO_OF_COMMENTS = "noOfComments";
    public static final String NO_OF_SHARES = "noOfShares";
    public static final String POSTED_TIME = "postedTime";


    // GetTopTenVideos
    public static final String FUN_GET_TOP_TEN_VIDEOS = "channel/getTopTenVideos";
    public static final String THUMBNAIL = "thumbnail";

    /// Get Generos Data Request
    public static final String FUN_GET_GENEROS_DATA = "videos/search";
    public static final String ORDER_BY = "orderBy";

    // Get Artistas Tab Data Request
    public static final String FUN_GET_ARTISTIAS = "getArtistos";
    public static final String ARTIST_ID = "artistId";
    public static final String ARTISTOS = "artistos";
    public static final String ARTIST = "artistName";
    public static final String AVATAR = "avatar";

    // Get Artist Data
    public static final String FUN_GET_ARTIST_DATA = "artist/info";
    public static final String AVATAR_URL = "avatarUrl";
    public static final String BIOGRAPHY  = "biography";
    public static final String OTHER_INFORMATION = "otherInformation";

    //  Get Farand
    public static final String FUN_GET_FARANDULAS = "getFarandulas";
    public static final String FARANDULAS = "farandulas";
    public static final String LOCATION = "location";
    public static final String DATE_TIME = "adddate";
    public static final String VOTES = "votes";
    public static final String ACCOUNT_TITLE = "title";

    /// Get Djs
    public static final String FUN_GET_DJS = "getDjs";
    public static final String DJS = "djs";
    public static final String ARTIST_NAME = "artistName";

    /// Get Music
    public static final String GET_MUSIC = "dj/info";
    public static final String DJ = "dj";
    public static final String  TRACKS = "tracks";
    public static final String  TRACK_TITLE = "trackTitle";
    public static final String  ALBUM_NAME = "albumName";
    public static final String  TRACK_TIME = "trackTime";
    public static final String IS_EXPLICIT = "isExplicit";
    public static final String PROFILE = "profile";

    // GET_MAS_VISTO
    public static final String FUN_GET_MAS_VISTO = "videos/masVistos";

    // FUN_GET_VISTO_AHORA;
    public static final String FUN_GET_VISTO_AHORA = "videos/vistosAhora";

    // Get_Artistas_Info_Request;
    public static final String FUN_GET_ARTISTAS_INFO = "artist/info";

    // send message
    public static final String FUN_SEND_MESSAGE = "contact";
    public static final String PHONE = "phone";
    public static final String MESSAGE = "message";
    public static final String  USERNAME = "username";
}
