package com.sts.asopao.Main;

import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.sts.asopao.APIs.SignupRequest;
import com.sts.asopao.Base.BaseActivity;
import com.sts.asopao.Common.Commons;
import com.sts.asopao.Common.Constants;
import com.sts.asopao.Common.ReqConst;
import com.sts.asopao.Models.UserModel;
import com.sts.asopao.Preference.PrefConst;
import com.sts.asopao.Preference.Preference;
import com.sts.asopao.R;

import org.json.JSONException;
import org.json.JSONObject;

public class SignupActivity extends BaseActivity {

    ImageView imvBack, imvViewPassword;
    EditText edtPassword, edtFirstName, edtLastName, edtEmail;
    Button btnSingup;

    String firstName = "", lastName = "", email = "", password = "", photoUrl = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        loadLayout();
        hideKeyboard();
    }

    private void loadLayout() {

        edtEmail = (EditText)findViewById(R.id.edtEmail);
        edtFirstName = (EditText)findViewById(R.id.edtFirstName);
        edtLastName = (EditText)findViewById(R.id.edtLastName);

        btnSingup = (Button)findViewById(R.id.btnSingup);
        btnSingup.setOnClickListener(this);

        imvBack = (ImageView)findViewById(R.id.imvBack);
        imvBack.setOnClickListener(this);

        edtPassword = (EditText)findViewById(R.id.edtPassword);

        imvViewPassword = (ImageView)findViewById(R.id.imvViewPassword);
        imvViewPassword.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int eventAction = event.getAction();
                switch (eventAction){
                    case MotionEvent.ACTION_DOWN:
                        imvViewPassword.setImageResource(R.drawable.look_password);
                        edtPassword.setInputType(InputType.TYPE_CLASS_TEXT);
                        break;
                    case MotionEvent.ACTION_UP:
                        imvViewPassword.setImageResource(R.drawable.unlook_password);
                        edtPassword.setInputType(InputType.TYPE_CLASS_TEXT|InputType.TYPE_TEXT_VARIATION_PASSWORD);
                        break;
                }
                return true;
            }
        });
    }

    private void gotoLoginWithEmailActivity() {

        Intent intent = new Intent(this, LoginWithEmailActivity.class);
        startActivity(intent);
        finish();
    }

    private void gotoMainActivity() {

        Preference.getInstance().put(this, PrefConst.EMAIL, email);
        Preference.getInstance().put(this, PrefConst.PASSWORD, password);

        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onBackPressed() {
        gotoLoginWithEmailActivity();
    }

    private boolean isValid() {

        firstName = edtFirstName.getText().toString().trim();
        lastName = edtLastName.getText().toString().trim();
        email = edtEmail.getText().toString().trim();
        password = edtPassword.getText().toString().trim();

        if (firstName.length() == 0){
            showAlertDialog("Please input first name");
            return false;
        }else if (lastName.length() == 0){
            showAlertDialog("Please input last name");
            return false;
        }else if (email.length() == 0){
            showAlertDialog("Please input email");
            return false;
        }else if (password.length() == 0){
            showAlertDialog("Please input password");
            return false;
        }

        return true;
    }

    private void processSignup() {
        final Response.Listener<String> res = new Response.Listener<String>() {
            @Override
            public void onResponse(String json) {
                closeProgress();
                try{
                    JSONObject response = new JSONObject(json);
                    int resultCode = response.getInt(ReqConst.RES_CODE);
                    if (resultCode == ReqConst.CODE_SUCCESS){

                        JSONObject jsonUser = response.getJSONObject(ReqConst.USER_MODEL);
                        UserModel userModel = new UserModel();
                        userModel.setId(jsonUser.getInt(ReqConst.ID));
                        userModel.setFirstName(jsonUser.getString(ReqConst.FIRST_NAME));
                        userModel.setLastName(jsonUser.getString(ReqConst.LAST_NAME));
                        userModel.setEmail(jsonUser.getString(ReqConst.EMAIL));
                        userModel.setPhotoUrl(jsonUser.getString(ReqConst.PHOTO_URL));

                        Commons.user = userModel;

                        gotoMainActivity();
                    }else {
                        showAlertDialog(getString(R.string.server_failed));
                    }
                }catch (JSONException e){
                    showAlertDialog(getString(R.string.server_failed));
                }
            }
        };
        final Response.ErrorListener error = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                closeProgress();
                showAlertDialog(getString(R.string.server_failed));
            }
        };

        showProgress();
        SignupRequest req = new SignupRequest(firstName, lastName, email, password, photoUrl, res, error);
        req.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue request = Volley.newRequestQueue(this);
        request.add(req);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.imvBack:
                gotoLoginWithEmailActivity();
                break;
            case R.id.btnSingup:
                if (isValid())
                    processSignup();
                break;
        }
    }
}
