package com.sts.asopao.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.makeramen.roundedimageview.RoundedImageView;
import com.squareup.picasso.Picasso;
import com.sts.asopao.Main.MainActivity;
import com.sts.asopao.Models.DJS_Model;
import com.sts.asopao.Models.VIDEO_MUSIC_Model;
import com.sts.asopao.R;

import java.util.ArrayList;

/**
 * Created by STS on 12/1/2017.
 */

public class VIDEO_MUSIC_Adapter extends BaseAdapter {

    MainActivity activity;

    ArrayList<VIDEO_MUSIC_Model> allData = new ArrayList<>();

    public VIDEO_MUSIC_Adapter(MainActivity activity) {
        this.activity = activity;
    }

    public void refresh(ArrayList<VIDEO_MUSIC_Model> data){
        allData.clear();
        allData.addAll(data);
        notifyDataSetChanged();
    }

    public int getSize(){
        return allData.size();
    }

    @Override
    public int getCount() {
        return allData.size();
    }

    @Override
    public VIDEO_MUSIC_Model getItem(int position) {
        return allData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        CustomHolder holder;
        if (convertView ==  null){
            holder =  new CustomHolder();
            LayoutInflater inflater = LayoutInflater.from(activity);
            convertView = inflater.inflate(R.layout.item_music_player, parent, false);
            holder.setId(convertView);
            convertView.setTag(holder);
        }else {
            holder = (CustomHolder)convertView.getTag();
        }

        VIDEO_MUSIC_Model data =  allData.get(position);
        holder.setData(data, position);

        return convertView;
    }

    private class CustomHolder implements View.OnClickListener{

        TextView txvTitle;
        ImageView imvThumbnail;
        private void setId(View view){
            txvTitle = (TextView)view.findViewById(R.id.txvTitle);
            imvThumbnail = (ImageView)view.findViewById(R.id.imvThumbnail);
        }

        private void setData(VIDEO_MUSIC_Model model, int position){
            txvTitle.setText(model.getTitle());

            if (model.getThumbnailUrl().length() > 0){
                Picasso.with(activity).load(model.getThumbnailUrl()).into(imvThumbnail);
            }
        }

        @Override
        public void onClick(View v) {

        }
    }
}
