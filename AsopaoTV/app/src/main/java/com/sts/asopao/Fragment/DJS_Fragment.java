package com.sts.asopao.Fragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.sts.asopao.APIs.Get_DJS_Request;
import com.sts.asopao.Adapter.DJS_Adapter;
import com.sts.asopao.Base.BaseFragment;
import com.sts.asopao.Common.Commons;
import com.sts.asopao.Common.Constants;
import com.sts.asopao.Common.ReqConst;
import com.sts.asopao.Main.MainActivity;
import com.sts.asopao.Models.DJS_Model;
import com.sts.asopao.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by STS on 12/1/2017.
 */

@SuppressLint("ValidFragment")
public class DJS_Fragment extends BaseFragment{

    MainActivity activity;
    DJS_Adapter mDJS_Adapter;

    GridView gvDJS;

    public DJS_Fragment(MainActivity activity) {
        this.activity = activity;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View fragment = inflater.inflate(R.layout.fragment_djs, container, false);

        mDJS_Adapter = new DJS_Adapter(activity);
        gvDJS = (GridView)fragment.findViewById(R.id.gvDJS);
        gvDJS.setAdapter(mDJS_Adapter);

        return fragment;
    }

    private void getAllData() {
        final Response.Listener<String> res = new Response.Listener<String>() {
            @Override
            public void onResponse(String json) {
                activity.closeProgress();
                try{
                    JSONObject response = new JSONObject(json);
                    int resultCode = response.getInt(ReqConst.RES_CODE);
                    if (resultCode == ReqConst.CODE_SUCCESS){

                        ArrayList<DJS_Model> mDJS_Model_List = new ArrayList<>();
                        JSONArray jsonDJS_Model_List = response.getJSONArray(ReqConst.DJS);

                        for (int i = 0; i < jsonDJS_Model_List.length(); i ++){
                            DJS_Model mDJS_Model =  new DJS_Model();
                            JSONObject jsonDJS_Model =  (JSONObject)jsonDJS_Model_List.get(i);

                            mDJS_Model.setArtistName(jsonDJS_Model.getString(ReqConst.ARTIST_NAME));
                            mDJS_Model.setChannelName(jsonDJS_Model.getString(ReqConst.CHANNEL_NAME));
                            mDJS_Model.setAvatar(jsonDJS_Model.getString(ReqConst.AVATAR));

                            mDJS_Model_List.add(mDJS_Model);
                        }

                        Commons.mDJS_Model_List.clear();
                        Commons.mDJS_Model_List.addAll(mDJS_Model_List);

                        mDJS_Adapter.refresh(mDJS_Model_List);

                    }else{
                        activity.showAlertDialog(getString(R.string.server_failed));
                    }

                }catch (JSONException e){
                    activity.showAlertDialog(getString(R.string.server_failed));
                }
            }
        };
        final Response.ErrorListener error = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                activity.closeProgress();
                activity.showAlertDialog(getString(R.string.server_failed));
            }
        };

        activity.showProgress();
        Get_DJS_Request req = new Get_DJS_Request(res, error);
        req.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue request = Volley.newRequestQueue(activity);
        request.add(req);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (Commons.mDJS_Model_List.size() > 0){
            mDJS_Adapter.refresh(Commons.mDJS_Model_List);
        }else {
            getAllData();
        }
    }
}
