package com.sts.asopao.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.sts.asopao.Main.MainActivity;
import com.sts.asopao.Models.TOP_10_Model;
import com.sts.asopao.Models.VIDEOS_Model;
import com.sts.asopao.R;

import java.util.ArrayList;

/**
 * Created by STS on 12/1/2017.
 */

public class VIDEOS_Adapter extends BaseAdapter {

    MainActivity activity;

    ArrayList<VIDEOS_Model> allData = new ArrayList<>();

    public VIDEOS_Adapter(MainActivity activity) {
        this.activity = activity;
    }

    public void refresh(ArrayList<VIDEOS_Model> data){
        allData.clear();
        allData.addAll(data);
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return allData.size();
    }

    @Override
    public VIDEOS_Model getItem(int position) {
        return allData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        CustomHolder holder;
        if (convertView ==  null){
            holder =  new CustomHolder();
            LayoutInflater inflater = LayoutInflater.from(activity);
            convertView = inflater.inflate(R.layout.item_videos, parent, false);
            holder.setId(convertView);
            convertView.setTag(holder);
        }else {
            holder = (CustomHolder)convertView.getTag();
        }

        VIDEOS_Model data =  allData.get(position);
        holder.setData(data);

        return convertView;
    }

    private class CustomHolder{

        ImageView imvThumbnail;
        TextView txvVideoName, txvChannelName, txvNumberOfViews;

        private void setId (View view){
            imvThumbnail = (ImageView)view.findViewById(R.id.imvThumbnail);
            txvVideoName = (TextView)view.findViewById(R.id.txvVideoName);
            txvChannelName = (TextView)view.findViewById(R.id.txvChannelName);
            txvNumberOfViews =  (TextView)view.findViewById(R.id.txvNumberOfViews);
        }

        private void setData(VIDEOS_Model data){
            if (data.getThumbnail().length() > 0){
                Picasso.with(activity).load(data.getThumbnail()).into(imvThumbnail);
            }

            txvVideoName.setText(data.getTitle());
            txvChannelName.setText(data.getChannelName());
            txvNumberOfViews.setText("+" + data.getNumberOfViews());
        }
    }
}
