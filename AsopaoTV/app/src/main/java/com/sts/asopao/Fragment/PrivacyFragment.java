package com.sts.asopao.Fragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.sts.asopao.APIs.Get_TOP_10_Request;
import com.sts.asopao.Adapter.TOP_10_Adapter;
import com.sts.asopao.Base.BaseFragment;
import com.sts.asopao.Common.Commons;
import com.sts.asopao.Common.Constants;
import com.sts.asopao.Common.PrivacyTermsClass;
import com.sts.asopao.Common.ReqConst;
import com.sts.asopao.Main.MainActivity;
import com.sts.asopao.Main.PrivacyActivity;
import com.sts.asopao.Models.TOP_10_Model;
import com.sts.asopao.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by STS on 12/1/2017.
 */

@SuppressLint("ValidFragment")
public class PrivacyFragment extends BaseFragment{

    PrivacyActivity activity;

    TextView txvPrivacy;

    public PrivacyFragment(PrivacyActivity activity) {
        this.activity = activity;
    }

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View fragment = inflater.inflate(R.layout.fragment_privacy, container, false);

        txvPrivacy = (TextView)fragment.findViewById(R.id.txvPrivacy);
        txvPrivacy.setText(PrivacyTermsClass.policy);

        return fragment;
    }
}
