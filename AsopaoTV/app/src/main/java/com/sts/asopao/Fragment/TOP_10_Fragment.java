package com.sts.asopao.Fragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.sts.asopao.APIs.Get_TOP_10_Request;
import com.sts.asopao.Adapter.TOP_10_Adapter;
import com.sts.asopao.Base.BaseFragment;
import com.sts.asopao.Common.Commons;
import com.sts.asopao.Common.Constants;
import com.sts.asopao.Common.ReqConst;
import com.sts.asopao.Main.MainActivity;
import com.sts.asopao.Models.TOP_10_Model;
import com.sts.asopao.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by STS on 12/1/2017.
 */

@SuppressLint("ValidFragment")
public class TOP_10_Fragment extends BaseFragment{

    MainActivity activity;
    TOP_10_Adapter mTOP_10_Adapter;

    ListView lstTop10;

    public TOP_10_Fragment(MainActivity activity) {
        this.activity = activity;
    }

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View fragment = inflater.inflate(R.layout.fragment_top_10, container, false);

        mTOP_10_Adapter =  new TOP_10_Adapter(activity);
        lstTop10 = (ListView)fragment.findViewById(R.id.lstTop10);
        lstTop10.setAdapter(mTOP_10_Adapter);
        lstTop10.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                int selectedChannelId = mTOP_10_Adapter.getItem(position).getChannelId();
                activity.showTop10Detail_Fragment(selectedChannelId);
            }
        });

        return fragment;
    }

    private void getAllData() {
        final Response.Listener<String> res = new Response.Listener<String>() {
            @Override
            public void onResponse(String json) {
                activity.closeProgress();
                try{
                    JSONObject response = new JSONObject(json);
                    int resultCode = response.getInt(ReqConst.RES_CODE);
                    if (resultCode == ReqConst.CODE_SUCCESS){

                        ArrayList<TOP_10_Model> mTOP_10_Model_List =  new ArrayList<>();
                        JSONArray jsonTOP_10_Model_List = response.getJSONArray(ReqConst.TOP_CHANNELS);

                        for (int i = 0 ; i < jsonTOP_10_Model_List.length(); i++){

                            TOP_10_Model mTOP_10_Model =  new TOP_10_Model();
                            JSONObject jsonTOP_10_Model =  (JSONObject)jsonTOP_10_Model_List.get(i);

                            mTOP_10_Model.setChannelId(jsonTOP_10_Model.getInt(ReqConst.CHANNEL_ID));
                            mTOP_10_Model.setChannelName(jsonTOP_10_Model.getString(ReqConst.CHANNEL_NAME));
                            mTOP_10_Model.setThumbnailUrl(jsonTOP_10_Model.getString(ReqConst.THUMBNAIL_URL));

                            mTOP_10_Model_List.add(mTOP_10_Model);
                        }

                        Commons.mTOP_10_Model_List.clear();
                        Commons.mTOP_10_Model_List.addAll(mTOP_10_Model_List);

                        mTOP_10_Adapter.refresh(mTOP_10_Model_List);

                    }else{
                        activity.showAlertDialog(getString(R.string.server_failed));
                    }

                }catch (JSONException e){
                    activity.showAlertDialog(getString(R.string.server_failed));
                }
            }
        };
        final Response.ErrorListener error = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                activity.closeProgress();
                activity.showAlertDialog(getString(R.string.server_failed));
            }
        };

        activity.showProgress();
        Get_TOP_10_Request req = new Get_TOP_10_Request(res, error);
        req.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue request = Volley.newRequestQueue(activity);
        request.add(req);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (Commons.mTOP_10_Model_List.size() > 0){
            mTOP_10_Adapter.refresh(Commons.mTOP_10_Model_List);
        }else {
            getAllData();
        }
    }
}
