package com.sts.asopao.APIs;

import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;
import com.sts.asopao.Common.ReqConst;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by STS on 2/9/2017.
 */

public class GetGenerosDataRequest extends StringRequest {

    private static  final String url =  ReqConst.SERVER_URL + ReqConst.FUN_GET_GENEROS_DATA;
    private Map<String,String> params;

    public GetGenerosDataRequest(String genreId, String orderBy, Response.Listener<String> listener, Response.ErrorListener error) {

        super(Method.POST, url, listener,error);

        params = new HashMap<>();
        params.put(ReqConst.GENEROS_ID, genreId);
        params.put(ReqConst.ORDER_BY, orderBy);
    }

    @Override
    public Map<String, String> getParams() {
        return params;
    }
}
