package com.sts.asopao.Common;

import com.sts.asopao.Models.ARTISTAS_Model;
import com.sts.asopao.Models.DJS_Model;
import com.sts.asopao.Models.ESTRENOS_Model;
import com.sts.asopao.Models.FARANDULA_Model;
import com.sts.asopao.Models.GENEROS_Model;
import com.sts.asopao.Models.MAS_VISTO_Model;
import com.sts.asopao.Models.TOP_10_Model;
import com.sts.asopao.Models.UserModel;
import com.sts.asopao.Models.VIDEOS_Model;
import com.sts.asopao.Models.VISTO_AHORA_Model;

import java.net.PortUnreachableException;
import java.util.ArrayList;

import cn.trinea.android.view.autoscrollviewpager.AutoScrollViewPager;

/**
 * Created by STS on 10/17/2017.
 */

public class Commons {
    public static boolean g_isAppRunning = false;
    public static UserModel user = new UserModel();
    public static ArrayList<ESTRENOS_Model> mESTRENOS_Model_List = new ArrayList<>();
    public static ArrayList<TOP_10_Model> mTOP_10_Model_List = new ArrayList<>();
    public static ArrayList<GENEROS_Model> mGENEROS_Model_List = new ArrayList<>();
    public static ArrayList<ARTISTAS_Model> mARTISTAS_Model_List =  new ArrayList<>();
    public static int currentPage = 1;
    public static ArrayList<VIDEOS_Model> mVIDEOS_Model_List = new ArrayList<>();
    public static ArrayList<DJS_Model> mDJS_Model_List =  new ArrayList<>();
    public static ArrayList<FARANDULA_Model> mFARANDULA_Model_List =  new ArrayList<>();
    public static int selectedVideoId = 0;
    public static ArrayList<MAS_VISTO_Model> mMAS_VISTO_Model_List  =  new ArrayList<>();
    public static ArrayList<VISTO_AHORA_Model> mVISTO_AHORA_Model_List = new ArrayList<>();
    public static String artistId = "";
    public static String artiastasPhotoUrl = "";
}