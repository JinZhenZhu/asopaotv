package com.sts.asopao.Fragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.sts.asopao.APIs.Get_FRANDULA_Request;
import com.sts.asopao.Adapter.FARANDULA_Adapter;
import com.sts.asopao.Base.BaseFragment;
import com.sts.asopao.Common.Commons;
import com.sts.asopao.Common.Constants;
import com.sts.asopao.Common.ReqConst;
import com.sts.asopao.Main.MainActivity;
import com.sts.asopao.Models.FARANDULA_Model;
import com.sts.asopao.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by STS on 12/1/2017.
 */

@SuppressLint("ValidFragment")
public class FARANDULA_Fragment extends BaseFragment{

    MainActivity activity;
    FARANDULA_Adapter mFARANDULA_Adapter;

    ListView lstFARANDULA;

    public FARANDULA_Fragment(MainActivity activity) {
        this.activity = activity;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View fragment = inflater.inflate(R.layout.fragment_farandula, container, false);

        mFARANDULA_Adapter = new FARANDULA_Adapter(activity);
        lstFARANDULA = (ListView)fragment.findViewById(R.id.lstFARANDULA);
        lstFARANDULA.setAdapter(mFARANDULA_Adapter);

        return fragment;
    }

    private void getAllData() {
        final Response.Listener<String> res = new Response.Listener<String>() {
            @Override
            public void onResponse(String json) {
                activity.closeProgress();
                try{
                    JSONObject response = new JSONObject(json);
                    int resultCode = response.getInt(ReqConst.RES_CODE);
                    if (resultCode == ReqConst.CODE_SUCCESS){

                        ArrayList<FARANDULA_Model> mFARANDULA_Model_List = new ArrayList<>();
                        JSONArray jsonFARANDULA_Model_List = response.getJSONArray(ReqConst.FARANDULAS);

                        for (int i = 0; i < jsonFARANDULA_Model_List.length(); i ++){
                            FARANDULA_Model mFARANDULA_Model =  new FARANDULA_Model();
                            JSONObject jsonFarandModel =  (JSONObject)jsonFARANDULA_Model_List.get(i);

                            mFARANDULA_Model.setTitle(jsonFarandModel.getString(ReqConst.ACCOUNT_TITLE));
//                            mFARANDULA_Model.setDateTime(jsonFarandModel.getString(ReqConst.DATE_TIME));
//                            mFARANDULA_Model.setNoOfVotes(jsonFarandModel.getString(ReqConst.NO_OF_VOTES));
//                            mFARANDULA_Model.setNoOfComments(jsonFarandModel.getString(ReqConst.NO_OF_COMMENTS));
//                            mFARANDULA_Model.setNoOfShares(jsonFarandModel.getString(ReqConst.NO_OF_SHARES));
//                            mFARANDULA_Model.setPostedTime(jsonFarandModel.getString(ReqConst.POSTED_TIME));
                            mFARANDULA_Model.setThumbnailUrl(jsonFarandModel.getString(ReqConst.THUMBNAIL_URL));

                            mFARANDULA_Model_List.add(mFARANDULA_Model);
                        }

                        Commons.mFARANDULA_Model_List.clear();
                        Commons.mFARANDULA_Model_List.addAll(mFARANDULA_Model_List);

                        mFARANDULA_Adapter.refresh(mFARANDULA_Model_List);

                    }else{
                        activity.showAlertDialog(getString(R.string.server_failed));
                    }

                }catch (JSONException e){
                    activity.showAlertDialog(getString(R.string.server_failed));
                }
            }
        };
        final Response.ErrorListener error = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                activity.closeProgress();
                activity.showAlertDialog(getString(R.string.server_failed));
            }
        };

        activity.showProgress();
        Get_FRANDULA_Request req = new Get_FRANDULA_Request(res, error);
        req.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue request = Volley.newRequestQueue(activity);
        request.add(req);
    }

    @Override
    public void onResume() {
        super.onResume();

        if (Commons.mFARANDULA_Model_List.size() > 0){
            mFARANDULA_Adapter.refresh(Commons.mFARANDULA_Model_List);
        }else {
            getAllData();
        }
    }
}
