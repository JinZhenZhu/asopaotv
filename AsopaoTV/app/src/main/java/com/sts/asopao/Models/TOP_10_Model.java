package com.sts.asopao.Models;

import java.io.Serializable;

/**
 * Created by STS on 12/1/2017.
 */

public class TOP_10_Model implements Serializable {
    int channelId = 0;
    String channelName = "", thumbnailUrl = "";

    public int getChannelId() {
        return channelId;
    }

    public void setChannelId(int channelId) {
        this.channelId = channelId;
    }

    public String getChannelName() {
        return channelName;
    }

    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }

    public String getThumbnailUrl() {
        return thumbnailUrl;
    }

    public void setThumbnailUrl(String thumbnailUrl) {
        this.thumbnailUrl = thumbnailUrl;
    }
}
