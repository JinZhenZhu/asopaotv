package com.sts.asopao.Fragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.sts.asopao.APIs.Get_ARTISTAS_Request;
import com.sts.asopao.Adapter.ARTISTAS_Adapter;
import com.sts.asopao.Base.BaseFragment;
import com.sts.asopao.Common.Commons;
import com.sts.asopao.Common.Constants;
import com.sts.asopao.Common.ReqConst;
import com.sts.asopao.Main.MainActivity;
import com.sts.asopao.Models.ARTISTAS_Model;
import com.sts.asopao.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by STS on 12/1/2017.
 */

@SuppressLint("ValidFragment")
public class ARTISTAS_Fragment extends BaseFragment{

    MainActivity activity;
    ARTISTAS_Adapter mARTISTAS_Adapter;

    GridView gvARTISTAS;

    public ARTISTAS_Fragment(MainActivity activity) {
        this.activity = activity;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View fragment = inflater.inflate(R.layout.fragment_artistas, container, false);

        mARTISTAS_Adapter = new ARTISTAS_Adapter(activity);
        gvARTISTAS = (GridView)fragment.findViewById(R.id.gvARTISTAS);
        gvARTISTAS.setAdapter(mARTISTAS_Adapter);
        gvARTISTAS.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                Commons.artistId = mARTISTAS_Adapter.getItem(position).getArtistId();
                Commons.artiastasPhotoUrl = mARTISTAS_Adapter.getItem(position).getAvatar();
                activity.showArtistasDetailFragment();
            }
        });

        return fragment;
    }

    private void getAllData() {
        final Response.Listener<String> res = new Response.Listener<String>() {
            @Override
            public void onResponse(String json) {
                activity.closeProgress();
                try{
                    JSONObject response = new JSONObject(json);
                    int resultCode = response.getInt(ReqConst.RES_CODE);
                    if (resultCode == ReqConst.CODE_SUCCESS){

                        ArrayList<ARTISTAS_Model> mARTISTAS_Model_List = new ArrayList<>();
                        JSONArray jsonARTISTAS_Model_List = response.getJSONArray(ReqConst.ARTISTOS);

                        for (int i = 0; i < jsonARTISTAS_Model_List.length(); i ++){
                            ARTISTAS_Model mARTISTAS_Model =  new ARTISTAS_Model();
                            JSONObject jsonARTISTAS_Model =  (JSONObject)jsonARTISTAS_Model_List.get(i);

                            mARTISTAS_Model.setArtistId(jsonARTISTAS_Model.getString(ReqConst.ARTIST_ID));
                            mARTISTAS_Model.setArtist(jsonARTISTAS_Model.getString(ReqConst.ARTIST));
                            mARTISTAS_Model.setChannelName(jsonARTISTAS_Model.getString(ReqConst.CHANNEL_NAME));
                            mARTISTAS_Model.setAvatar(jsonARTISTAS_Model.getString(ReqConst.AVATAR_URL));

                            mARTISTAS_Model_List.add(mARTISTAS_Model);
                        }

                        Commons.mARTISTAS_Model_List.clear();
                        Commons.mARTISTAS_Model_List.addAll(mARTISTAS_Model_List);

                        mARTISTAS_Adapter.refresh(mARTISTAS_Model_List);

                    }else{
                        activity.showAlertDialog(getString(R.string.server_failed));
                    }

                }catch (JSONException e){
                    activity.showAlertDialog(getString(R.string.server_failed));
                }
            }
        };
        final Response.ErrorListener error = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                activity.closeProgress();
                activity.showAlertDialog(getString(R.string.server_failed));
            }
        };

        activity.showProgress();
        Get_ARTISTAS_Request req = new Get_ARTISTAS_Request(res, error);
        req.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue request = Volley.newRequestQueue(activity);
        request.add(req);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (Commons.mARTISTAS_Model_List.size() > 0){
            mARTISTAS_Adapter.refresh(Commons.mARTISTAS_Model_List);
        }else {
            getAllData();
        }
    }
}
