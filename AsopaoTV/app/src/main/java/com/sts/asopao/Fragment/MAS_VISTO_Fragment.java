package com.sts.asopao.Fragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.sts.asopao.APIs.Get_ESTRENOS_Request;
import com.sts.asopao.APIs.Get_MAS_VISTO_Request;
import com.sts.asopao.Adapter.MAS_VISTO_Adapter;
import com.sts.asopao.Base.BaseFragment;
import com.sts.asopao.Common.Commons;
import com.sts.asopao.Common.Constants;
import com.sts.asopao.Common.ReqConst;
import com.sts.asopao.Main.MainActivity;
import com.sts.asopao.Models.ESTRENOS_Model;
import com.sts.asopao.Models.MAS_VISTO_Model;
import com.sts.asopao.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by STS on 12/1/2017.
 */

@SuppressLint("ValidFragment")
public class MAS_VISTO_Fragment extends BaseFragment{

    MainActivity activity;
    MAS_VISTO_Adapter mMAS_VISTO_Adapter;

    ListView lstMAS_VISTO;

    public MAS_VISTO_Fragment(MainActivity activity) {
        this.activity = activity;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View fragment = inflater.inflate(R.layout.fragment_mas_visto, container, false);

        mMAS_VISTO_Adapter =  new MAS_VISTO_Adapter(activity);
        lstMAS_VISTO = (ListView)fragment.findViewById(R.id.lstMAS_VISTO);
        lstMAS_VISTO.setAdapter(mMAS_VISTO_Adapter);
        lstMAS_VISTO.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                Commons.selectedVideoId = Integer.valueOf(mMAS_VISTO_Adapter.getItem(position).getVideoId());
                activity.showVideoMusicalFragment();
            }
        });

        return fragment;
    }

    private void getAllData() {
        final Response.Listener<String> res = new Response.Listener<String>() {
            @Override
            public void onResponse(String json) {
                activity.closeProgress();
                try{
                    JSONObject response = new JSONObject(json);
                    int resultCode = response.getInt(ReqConst.RES_CODE);
                    if (resultCode == ReqConst.CODE_SUCCESS){

                        ArrayList<MAS_VISTO_Model> mMAS_VISTO_Model_List =  new ArrayList<>();
                        JSONArray jsonMAS_VISTO_Model_List = response.getJSONArray(ReqConst.VIDEOS);

                        for (int i = 0 ; i < jsonMAS_VISTO_Model_List.length(); i++){
                            MAS_VISTO_Model mESTRENOS_Model =  new MAS_VISTO_Model();
                            JSONObject jsonMAS_VISTO_Model =  (JSONObject)jsonMAS_VISTO_Model_List.get(i);

                            mESTRENOS_Model.setVideoId(jsonMAS_VISTO_Model.getString(ReqConst.VIDEO_ID));
                            mESTRENOS_Model.setTitle(jsonMAS_VISTO_Model.getString(ReqConst.TITLE));
                            mESTRENOS_Model.setDuration(jsonMAS_VISTO_Model.getString(ReqConst.DURATION));
                            mESTRENOS_Model.setNumberOfViews(jsonMAS_VISTO_Model.getString(ReqConst. NUMBER_OF_VIEWS));
                            mESTRENOS_Model.setNoOfLikes(jsonMAS_VISTO_Model.getString(ReqConst.NO_OF_LIKES));
                            mESTRENOS_Model.setChannelName(jsonMAS_VISTO_Model.getString(ReqConst.CHANNEL_NAME));

                            JSONArray jsonThumbnailUrlList = jsonMAS_VISTO_Model.getJSONArray(ReqConst.THUMBNAIL_URLS);
                            ArrayList<String> thumbnailList = new ArrayList<>();
                            for (int j = 0; j < jsonThumbnailUrlList.length(); j ++){
                                String temp =  jsonThumbnailUrlList.get(j).toString();
                                thumbnailList.add(temp);
                            }

                            mESTRENOS_Model.setThumbnailUrlList(thumbnailList);

                            mMAS_VISTO_Model_List.add(mESTRENOS_Model);
                        }

                        Commons.mMAS_VISTO_Model_List.clear();
                        Commons.mMAS_VISTO_Model_List.addAll(mMAS_VISTO_Model_List);
                        mMAS_VISTO_Adapter.refresh(mMAS_VISTO_Model_List);

                    }else{
                        activity.showAlertDialog(getString(R.string.server_failed));
                    }

                }catch (JSONException e){
                    activity.showAlertDialog(getString(R.string.server_failed));
                }
            }
        };
        final Response.ErrorListener error = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                activity.closeProgress();
                activity.showAlertDialog(getString(R.string.server_failed));
            }
        };

        activity.showProgress();
        Get_MAS_VISTO_Request req = new Get_MAS_VISTO_Request(res, error);
        req.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue request = Volley.newRequestQueue(activity);
        request.add(req);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (Commons.mMAS_VISTO_Model_List.size() > 0){
            mMAS_VISTO_Adapter.refresh(Commons.mMAS_VISTO_Model_List);
        }else {
            getAllData();
        }
    }
}
