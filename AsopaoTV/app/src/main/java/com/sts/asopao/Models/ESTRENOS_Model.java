package com.sts.asopao.Models;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by STS on 12/1/2017.
 */

public class ESTRENOS_Model implements Serializable {
    int videoId = 0;
    String title = "", duration = "", videoName = "", createdTime = "",
            thumbnail = "", numberOfViews = "", rating = "", noOfLikes = "",
            noOfDislikes = "", videoType = "",
            HD = "", channelName = "", key = "";

    ArrayList<String> thumbnailUrlList = new ArrayList<>();

    public int getVideoId() {
        return videoId;
    }

    public void setVideoId(int videoId) {
        this.videoId = videoId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getVideoName() {
        return videoName;
    }

    public void setVideoName(String videoName) {
        this.videoName = videoName;
    }

    public String getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(String createdTime) {
        this.createdTime = createdTime;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getNumberOfViews() {
        return numberOfViews;
    }

    public void setNumberOfViews(String numberOfViews) {
        this.numberOfViews = numberOfViews;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getNoOfLikes() {
        return noOfLikes;
    }

    public void setNoOfLikes(String noOfLikes) {
        this.noOfLikes = noOfLikes;
    }

    public String getNoOfDislikes() {
        return noOfDislikes;
    }

    public void setNoOfDislikes(String noOfDislikes) {
        this.noOfDislikes = noOfDislikes;
    }

    public String getVideoType() {
        return videoType;
    }

    public void setVideoType(String videoType) {
        this.videoType = videoType;
    }

    public String getHD() {
        return HD;
    }

    public void setHD(String HD) {
        this.HD = HD;
    }

    public String getChannelName() {
        return channelName;
    }

    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public ArrayList<String> getThumbnailUrlList() {
        return thumbnailUrlList;
    }

    public void setThumbnailUrlList(ArrayList<String> thumbnailUrlList) {
        this.thumbnailUrlList = thumbnailUrlList;
    }
}
