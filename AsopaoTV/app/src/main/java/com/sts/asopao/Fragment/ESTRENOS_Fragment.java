package com.sts.asopao.Fragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.sts.asopao.APIs.Get_ESTRENOS_Request;
import com.sts.asopao.Adapter.ESTRENOS_Adapter;
import com.sts.asopao.Base.BaseFragment;
import com.sts.asopao.Common.Commons;
import com.sts.asopao.Common.Constants;
import com.sts.asopao.Common.ReqConst;
import com.sts.asopao.Main.MainActivity;
import com.sts.asopao.Models.ESTRENOS_Model;
import com.sts.asopao.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by STS on 12/1/2017.
 */

@SuppressLint("ValidFragment")
public class ESTRENOS_Fragment extends BaseFragment{

    MainActivity activity;

    ListView lstEstrenos;
    ESTRENOS_Adapter mESTRENOS_Adapter;

    public ESTRENOS_Fragment(MainActivity activity) {
        this.activity = activity;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View fragment = inflater.inflate(R.layout.fragment_estrenos, container, false);

        mESTRENOS_Adapter = new ESTRENOS_Adapter(activity);
        lstEstrenos = (ListView)fragment.findViewById(R.id.lstEstrenos);
        lstEstrenos.setAdapter(mESTRENOS_Adapter);
        lstEstrenos.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Commons.selectedVideoId = mESTRENOS_Adapter.getItem(position).getVideoId();
                activity.showVideoMusicalFragment();
            }
        });

        return fragment;
    }

    private void getAllData() {
        final Response.Listener<String> res = new Response.Listener<String>() {
            @Override
            public void onResponse(String json) {
                activity.closeProgress();
                try{
                    JSONObject response = new JSONObject(json);
                    int resultCode = response.getInt(ReqConst.RES_CODE);
                    if (resultCode == ReqConst.CODE_SUCCESS){

                        ArrayList<ESTRENOS_Model> mESTRENOS_Model_List =  new ArrayList<>();
                        JSONArray jsonESTRENOS_Model_List = response.getJSONArray(ReqConst.VIDEOS);

                        for (int i = 0 ; i < jsonESTRENOS_Model_List.length(); i++){
                            ESTRENOS_Model mESTRENOS_Model =  new ESTRENOS_Model();
                            JSONObject jsonESTRENOS_Model =  (JSONObject)jsonESTRENOS_Model_List.get(i);

                            mESTRENOS_Model.setVideoId(jsonESTRENOS_Model.getInt(ReqConst.VIDEO_ID));
                            mESTRENOS_Model.setTitle(jsonESTRENOS_Model.getString(ReqConst.TITLE));
                            mESTRENOS_Model.setDuration(jsonESTRENOS_Model.getString(ReqConst.DURATION));
                            mESTRENOS_Model.setVideoName(jsonESTRENOS_Model.getString(ReqConst.VIDEO_NAME));
                            mESTRENOS_Model.setNumberOfViews(jsonESTRENOS_Model.getString(ReqConst. NUMBER_OF_VIEWS));
                            mESTRENOS_Model.setRating(jsonESTRENOS_Model.getString(ReqConst.RATING));
                            mESTRENOS_Model.setNoOfLikes(jsonESTRENOS_Model.getString(ReqConst.NO_OF_LIKES));
                            mESTRENOS_Model.setVideoType(jsonESTRENOS_Model.getString(ReqConst.VIDEO_TYPE));
                            mESTRENOS_Model.setHD(jsonESTRENOS_Model.getString(ReqConst.HD));
                            mESTRENOS_Model.setChannelName(jsonESTRENOS_Model.getString(ReqConst.CHANNEL_NAME));
                            mESTRENOS_Model.setKey(jsonESTRENOS_Model.getString(ReqConst.KEY));

                            JSONArray jsonThumbnailUrlList = jsonESTRENOS_Model.getJSONArray(ReqConst.THUMBNAIL_URLS);
                            ArrayList<String> thumbnailList = new ArrayList<>();
                            for (int j = 0; j < jsonThumbnailUrlList.length(); j ++){
                                String temp =  jsonThumbnailUrlList.get(j).toString();
                                thumbnailList.add(temp);
                            }

                            mESTRENOS_Model.setThumbnailUrlList(thumbnailList);

                            mESTRENOS_Model_List.add(mESTRENOS_Model);
                        }

                        Commons.mESTRENOS_Model_List.clear();
                        Commons.mESTRENOS_Model_List.addAll(mESTRENOS_Model_List);
                        mESTRENOS_Adapter.refresh(mESTRENOS_Model_List);

                    }else{
                        activity.showAlertDialog(getString(R.string.server_failed));
                    }

                }catch (JSONException e){
                    activity.showAlertDialog(getString(R.string.server_failed));
                }
            }
        };
        final Response.ErrorListener error = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                activity.closeProgress();
                activity.showAlertDialog(getString(R.string.server_failed));
            }
        };

        activity.showProgress();
        Get_ESTRENOS_Request req = new Get_ESTRENOS_Request(res, error);
        req.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue request = Volley.newRequestQueue(activity);
        request.add(req);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (Commons.mESTRENOS_Model_List.size() > 0){
            mESTRENOS_Adapter.refresh(Commons.mESTRENOS_Model_List);
        }else {
            getAllData();
        }
    }
}
