package com.sts.asopao.Main;

import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.BaseAdapter;

import com.facebook.login.Login;
import com.sts.asopao.Base.BaseActivity;
import com.sts.asopao.Common.Constants;
import com.sts.asopao.CustomView.SeekArc;
import com.sts.asopao.Preference.PrefConst;
import com.sts.asopao.Preference.Preference;
import com.sts.asopao.R;

import java.net.URL;
import java.net.URLConnection;
import java.util.Timer;
import java.util.TimerTask;

public class WelcomeActivity extends BaseActivity {

    MediaPlayer mediaPlayer;
    SeekArc musicProgressbar;

    TimerTask task;
    Timer timer;
    int duration = 0, songId = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);

        gotoLoginActivity();

        /*songId = Preference.getInstance().getValue(this, PrefConst.WELCOME_SONG_ID, 0);
        if (songId == 2){
            Preference.getInstance().put(this, PrefConst.WELCOME_SONG_ID, 0);
        }else {
            int id = songId + 1;
            Preference.getInstance().put(this, PrefConst.WELCOME_SONG_ID, id);
        }

        loadLayout();*/
    }

    private void loadLayout() {

        musicProgressbar = (SeekArc)findViewById(R.id.musicProgressbar);

        playMusic();

        task = new TimerTask() {
            @Override
            public void run() {
                if (mediaPlayer != null){
                    updateProgressBar();
                }
            }
        };
        timer = new Timer();
        timer.scheduleAtFixedRate(task, 0, 1000);

    }

    private void gotoLoginActivity() {
        Intent intent = new Intent(WelcomeActivity.this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    private void playMusic() {
        mediaPlayer = new MediaPlayer();
        int resId = Constants.WELCOME_SONG_LIST[songId];
        mediaPlayer = MediaPlayer.create(this, resId);
        mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        mediaPlayer.setLooping(false);
        mediaPlayer.start();

        Uri url = Uri.parse("android.resource://" + getPackageName() + "/" + resId);
        MediaMetadataRetriever mData = new MediaMetadataRetriever();
        mData.setDataSource(this, url);
        duration = Integer.parseInt(mData.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION));

        musicProgressbar.setMax(duration);
    }

    private void updateProgressBar() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (mediaPlayer != null){
                    int mCurrentPosition  = mediaPlayer.getCurrentPosition();
                    musicProgressbar.setProgress(mCurrentPosition);
                    if (mCurrentPosition > duration - 2000){
                        gotoLoginActivity();
                    }
                }
            }
        });

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        /*mediaPlayer.stop();
        timer.cancel();
        task.cancel();*/
    }
}


