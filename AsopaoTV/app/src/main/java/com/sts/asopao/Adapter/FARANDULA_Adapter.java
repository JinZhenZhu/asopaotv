package com.sts.asopao.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.sts.asopao.Main.MainActivity;
import com.sts.asopao.Models.FARANDULA_Model;
import com.sts.asopao.Models.TOP_10_Model;
import com.sts.asopao.R;

import java.util.ArrayList;

/**
 * Created by STS on 12/1/2017.
 */

public class FARANDULA_Adapter extends BaseAdapter {

    MainActivity activity;

    ArrayList<FARANDULA_Model> allData = new ArrayList<>();

    public FARANDULA_Adapter(MainActivity activity) {
        this.activity = activity;
    }

    public void refresh(ArrayList<FARANDULA_Model> data){
        allData.clear();
        allData.addAll(data);
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return allData.size();
    }

    @Override
    public Object getItem(int position) {
        return allData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        CustomHolder holder;
        if (convertView ==  null){
            holder =  new CustomHolder();
            LayoutInflater inflater = LayoutInflater.from(activity);
            convertView = inflater.inflate(R.layout.item_farandula, parent, false);
            holder.setId(convertView);
            convertView.setTag(holder);
        }else {
            holder = (CustomHolder)convertView.getTag();
        }

        FARANDULA_Model data = allData.get(position);
        holder.setData(data);

        return convertView;
    }

    private class CustomHolder{

        TextView txvNoOfComment, txvNoOfShare, txvNoOfVotes, txvLocation, txvPostedTime, txvDateTime;
        ImageView imvThumbnail;

        private void setId (View view){
            txvNoOfComment = (TextView)view.findViewById(R.id.txvNoOfComment);
            txvNoOfShare = (TextView)view.findViewById(R.id.txvNoOfShare);
            txvNoOfVotes = (TextView)view.findViewById(R.id.txvNoOfVotes);
            txvLocation = (TextView)view.findViewById(R.id.txvLocation);
            txvPostedTime = (TextView)view.findViewById(R.id.txvPostedTime);
            txvDateTime =  (TextView)view.findViewById(R.id.txvDateTime);

            imvThumbnail =  (ImageView)view.findViewById(R.id.imvThumbnail);
        }

        private void setData(FARANDULA_Model model){

            txvNoOfComment.setText(model.getNoOfComments());
            txvNoOfShare.setText(model.getNoOfShares());
            txvNoOfVotes.setText(model.getNoOfVotes());
            txvLocation.setText(model.getLocation());
            txvPostedTime.setText(model.getPostedTime());
            txvDateTime.setText(model.getDateTime());

            if (model.getThumbnailUrl().length() > 0){
                Picasso.with(activity).load(model.getThumbnailUrl()).into(imvThumbnail);
            }
        }
    }
}
