package com.sts.asopao.Models;

import java.io.Serializable;

/**
 * Created by STS on 12/1/2017.
 */

public class GENEROS_Model implements Serializable {

    int genreId = 0;
    String name = "", thumbnailUrl = "";

    public int getGenreId() {
        return genreId;
    }

    public void setGenreId(int genreId) {
        this.genreId = genreId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getThumbnailUrl() {
        return thumbnailUrl;
    }

    public void setThumbnailUrl(String thumbnailUrl) {
        this.thumbnailUrl = thumbnailUrl;
    }
}
