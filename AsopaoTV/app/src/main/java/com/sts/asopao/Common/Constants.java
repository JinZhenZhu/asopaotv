package com.sts.asopao.Common;

import com.sts.asopao.R;

import java.net.PortUnreachableException;

/**
 * Created by STS on 10/17/2017.
 */

public class Constants {
    public static final int SPLASH_TIME = 2000;
    public static final int VOLLEY_TIME_OUT = 120000;
    public static final String[] TAB_TITLES = {"Inicio", "Farandula", "Artistas", "Videos", "Generos",
            "Top 10", "Mas Visto", "Visto Ahora"};

    public static final Integer [] WELCOME_SONG_LIST = {R.raw.music_1, R.raw.music_2, R.raw.music_3};
    public static final String MOST_RECENT = "mostRecent";

    public static final int PICK_FROM_CAMERA = 100;
    public static final int PICK_FROM_ALBUM = 101;
    public static final int CROP_FROM_CAMERA = 102;
    public static final int MY_PEQUEST_CODE = 104;
    public static final int PROFILE_IMAGE_SIZE = 256;
}
