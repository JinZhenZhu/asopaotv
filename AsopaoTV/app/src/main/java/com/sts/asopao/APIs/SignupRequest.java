package com.sts.asopao.APIs;

import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;
import com.sts.asopao.Common.ReqConst;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by STS on 2/9/2017.
 */

public class SignupRequest extends StringRequest {

    private static  final String url =  ReqConst.SERVER_URL + ReqConst.FUN_SIGNUP;
    private Map<String,String> params;

    public SignupRequest(String firstName, String lastName, String email, String password, String photoUrl , Response.Listener<String> listener, Response.ErrorListener error) {

        super(Method.POST, url, listener,error);

        params = new HashMap<>();
        params.put(ReqConst.FIRST_NAME, firstName);
        params.put(ReqConst.LAST_NAME, lastName);
        params.put(ReqConst.EMAIL, email);
        params.put(ReqConst.PASSWORD, password);
        params.put(ReqConst.PHOTO_URL, photoUrl);
    }

    @Override
    public Map<String, String> getParams() {
        return params;
    }
}
