package com.sts.asopao.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.sts.asopao.Main.MainActivity;
import com.sts.asopao.Models.Top10VideoModel;
import com.sts.asopao.R;

import java.util.ArrayList;

/**
 * Created by STS on 10/19/2017.
 */

public class Top10Adapter extends BaseAdapter {

    MainActivity activity;

    ArrayList<Top10VideoModel> allData = new ArrayList<>();

    public Top10Adapter(MainActivity activity) {
        this.activity = activity;
    }

    public void refresh(ArrayList<Top10VideoModel> data){
        allData.clear();
        allData.addAll(data);
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return allData.size();
    }

    @Override
    public Object getItem(int position) {
        return allData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        CustomHolder holder;

        if (convertView == null){

            holder = new CustomHolder();
            LayoutInflater inflater = LayoutInflater.from(activity);
            convertView = inflater.inflate(R.layout.item_player, parent, false);

            holder.setId(convertView);
            convertView.setTag(holder);
        }else {
            holder = (CustomHolder)convertView.getTag();
        }

        Top10VideoModel model = allData.get(position);
        holder.setData(model, position);

        return convertView;
    }

    private class CustomHolder implements View.OnClickListener{

        LinearLayout lytContainer;
        TextView txvRowId, txvTitle, txvVotos;
        ImageView imvThumbnail;

        private void setId(View view){
            txvRowId = (TextView)view.findViewById(R.id.txvRowId);
            txvTitle = (TextView)view.findViewById(R.id.txvTitle);
            txvVotos = (TextView)view.findViewById(R.id.txvVotos);
            imvThumbnail = (ImageView)view.findViewById(R.id.imvThumbnail);
            lytContainer = (LinearLayout)view.findViewById(R.id.lytContainer);
        }

        private void setData(Top10VideoModel model, int position){

            if (position % 2 == 0){
                lytContainer.setBackgroundColor(activity.getResources().getColor(R.color.colorTop10_0));
            }else {
                lytContainer.setBackgroundColor(activity.getResources().getColor(R.color.colorTop10_1));
            }

            txvRowId.setText(String.valueOf(position + 1));
            txvTitle.setText(model.getTitle());

            if (model.getNoOfVotes().equals("1")){
                txvVotos.setText(model.getNoOfVotes() + " voto");
            }else {
                txvVotos.setText(model.getNoOfVotes() + " votos");
            }

            if (model.getThumbnailUrl().length() > 0){
                Picasso.with(activity).load(model.getThumbnailUrl()).into(imvThumbnail);
            }
        }

        @Override
        public void onClick(View v) {

        }
    }
}
