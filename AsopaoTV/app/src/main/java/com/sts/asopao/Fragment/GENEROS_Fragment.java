package com.sts.asopao.Fragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.sts.asopao.APIs.Get_GENEROS_Request;
import com.sts.asopao.Adapter.GENEROS_Adapter;
import com.sts.asopao.Base.BaseFragment;
import com.sts.asopao.Common.Commons;
import com.sts.asopao.Common.Constants;
import com.sts.asopao.Common.ReqConst;
import com.sts.asopao.Main.MainActivity;
import com.sts.asopao.Models.GENEROS_Model;
import com.sts.asopao.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by STS on 12/1/2017.
 */

@SuppressLint("ValidFragment")
public class GENEROS_Fragment extends BaseFragment{

    MainActivity activity;
    GENEROS_Adapter mGENEROS_Adapter;

    ListView lstGENEROS;

    public GENEROS_Fragment(MainActivity activity) {
        this.activity = activity;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View fragment = inflater.inflate(R.layout.fragment_generos, container, false);

        mGENEROS_Adapter = new GENEROS_Adapter(activity);
        lstGENEROS = (ListView)fragment.findViewById(R.id.lstGENEROS);
        lstGENEROS.setAdapter(mGENEROS_Adapter);
        lstGENEROS.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                activity.showGenerosDetailFragment(mGENEROS_Adapter.getItem(position).getGenreId());
            }
        });

        return fragment;
    }

    private void getAllData() {
        final Response.Listener<String> res = new Response.Listener<String>() {
            @Override
            public void onResponse(String json) {
                activity.closeProgress();
                try{
                    JSONObject response = new JSONObject(json);
                    int resultCode = response.getInt(ReqConst.RES_CODE);
                    if (resultCode == ReqConst.CODE_SUCCESS){

                        ArrayList<GENEROS_Model> mGENEROS_Model_List =  new ArrayList<>();
                        JSONArray jsonGENEROS_Model_List =  response.getJSONArray(ReqConst.GENEROS);

                        for (int i = 0; i < jsonGENEROS_Model_List.length(); i ++){
                            GENEROS_Model mGENEROS_Model =  new GENEROS_Model();
                            JSONObject jsonGENEROS_Model =  (JSONObject) jsonGENEROS_Model_List.get(i);

                            mGENEROS_Model.setGenreId(jsonGENEROS_Model.getInt(ReqConst.GENEROS_ID));
                            mGENEROS_Model.setName(jsonGENEROS_Model.getString(ReqConst.NAME));
                            mGENEROS_Model.setThumbnailUrl(jsonGENEROS_Model.getString(ReqConst.THUMBNAIL_URL));

                            mGENEROS_Model_List.add(mGENEROS_Model);
                        }

                        Commons.mGENEROS_Model_List.clear();
                        Commons.mGENEROS_Model_List.addAll(mGENEROS_Model_List);

                        mGENEROS_Adapter.refresh(mGENEROS_Model_List);

                    }else{
                        activity.showAlertDialog(getString(R.string.server_failed));
                    }

                }catch (JSONException e){
                    activity.showAlertDialog(getString(R.string.server_failed));
                }
            }
        };
        final Response.ErrorListener error = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                activity.closeProgress();
                activity.showAlertDialog(getString(R.string.server_failed));
            }
        };

        activity.showProgress();
        Get_GENEROS_Request req = new Get_GENEROS_Request(res, error);
        req.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue request = Volley.newRequestQueue(activity);
        request.add(req);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (Commons.mGENEROS_Model_List.size() > 0){
            mGENEROS_Adapter.refresh(Commons.mGENEROS_Model_List);
        }else {
            getAllData();
        }
    }
}
