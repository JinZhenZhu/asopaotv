package com.sts.asopao.APIs;

import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;
import com.sts.asopao.Common.ReqConst;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by STS on 2/9/2017.
 */

public class SendMessageRequest extends StringRequest {

    private static  final String url =  ReqConst.SERVER_URL + ReqConst.FUN_SEND_MESSAGE;
    private Map<String,String> params;

    public SendMessageRequest(String firstName, String lastName, String email, String phone, String message , Response.Listener<String> listener, Response.ErrorListener error) {

        super(Method.POST, url, listener,error);

        params = new HashMap<>();
        params.put(ReqConst.FIRST_NAME, firstName);
        params.put(ReqConst.LAST_NAME, lastName);
        params.put(ReqConst.EMAIL, email);
        params.put(ReqConst.PHONE, phone);
        params.put(ReqConst.MESSAGE, message);
    }

    @Override
    public Map<String, String> getParams() {
        return params;
    }
}
