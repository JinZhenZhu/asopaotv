package com.sts.asopao.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.sts.asopao.Main.MainActivity;
import com.sts.asopao.Models.GenerosDetailModel;
import com.sts.asopao.R;

import java.util.ArrayList;

/**
 * Created by STS on 10/19/2017.
 */

public class GenerosDetailAdapter extends BaseAdapter {

    MainActivity activity;

    ArrayList<GenerosDetailModel> allData = new ArrayList<>();

    public GenerosDetailAdapter(MainActivity activity) {
        this.activity = activity;
    }

    public void refresh(ArrayList<GenerosDetailModel> data){
        allData.clear();
        allData.addAll(data);
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return allData.size();
    }

    @Override
    public Object getItem(int position) {
        return allData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        CustomHolder holder;

        if (convertView == null){

            holder = new CustomHolder();
            LayoutInflater inflater = LayoutInflater.from(activity);
            convertView = inflater.inflate(R.layout.item_generos_detail, parent, false);

            holder.setId(convertView);
            convertView.setTag(holder);
        }else {
            holder = (CustomHolder)convertView.getTag();
        }

        GenerosDetailModel model = allData.get(position);
        holder.setData(model);

        return convertView;
    }

    private class CustomHolder implements View.OnClickListener{

        ImageView imvVideoThumbnail;
        TextView txvTitle, txvChannelName, txvNumberOfViews ;

        private void setId(View view){
            imvVideoThumbnail = (ImageView)view.findViewById(R.id.imvVideoThumbnail);
            txvTitle = (TextView)view.findViewById(R.id.txvTitle);
            txvChannelName = (TextView)view.findViewById(R.id.txvChannelName);
            txvNumberOfViews = (TextView)view.findViewById(R.id.txvNumberOfViews);
        }

        private void setData(GenerosDetailModel model){
            if (model.getThumbnailUrl().length() > 0){
                Picasso.with(activity).load(model.getThumbnailUrl()).into(imvVideoThumbnail);
            }

            txvTitle.setText(model.getTitle());
            txvChannelName.setText(model.getChannelName());
            txvNumberOfViews.setText("+" + model.getNumberOfViews());
        }

        @Override
        public void onClick(View v) {

        }
    }
}
