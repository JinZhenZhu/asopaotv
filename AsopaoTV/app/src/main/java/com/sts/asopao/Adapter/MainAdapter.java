package com.sts.asopao.Adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.sts.asopao.Common.Constants;
import com.sts.asopao.Fragment.ARTISTAS_Fragment;
import com.sts.asopao.Fragment.DJS_Fragment;
import com.sts.asopao.Fragment.ESTRENOS_Fragment;
import com.sts.asopao.Fragment.FARANDULA_Fragment;
import com.sts.asopao.Fragment.GENEROS_Fragment;
import com.sts.asopao.Fragment.MAS_VISTO_Fragment;
import com.sts.asopao.Fragment.TOP_10_Fragment;
import com.sts.asopao.Fragment.VIDEOS_Fragment;
import com.sts.asopao.Fragment.VISTO_AHORA_Fragment;
import com.sts.asopao.Main.MainActivity;

/**
 * Created by STS on 12/1/2017.
 */

public class MainAdapter extends FragmentPagerAdapter {

    MainActivity activity;

    public MainAdapter(FragmentManager fm, MainActivity activity) {
        super(fm);
        this.activity = activity;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return Constants.TAB_TITLES[position];
    }

    @Override
    public int getCount() {
        return Constants.TAB_TITLES.length;
    }

    @Override
    public Fragment getItem(int position) {

        Fragment fragment;
        switch (position){
            case 0:
                fragment =  new ESTRENOS_Fragment(activity);
                break;
            case 1:
                fragment = new FARANDULA_Fragment(activity);
                break;
            case 2:
                fragment =  new ARTISTAS_Fragment(activity);
                break;
            case 3:
                fragment =  new VIDEOS_Fragment(activity);
                break;
            case 4:
                fragment = new GENEROS_Fragment(activity);
                break;
            case 6:
                fragment = new TOP_10_Fragment(activity);
                break;
            case 7:
                fragment = new MAS_VISTO_Fragment(activity);
                break;
            default:
                fragment = new VISTO_AHORA_Fragment(activity);
                break;
        }
        return fragment;
    }
}
