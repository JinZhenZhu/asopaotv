package com.sts.asopao.APIs;

import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;
import com.sts.asopao.Common.ReqConst;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by STS on 2/9/2017.
 */

public class GetTop10VideosRequest extends StringRequest {

    private static  final String url =  ReqConst.SERVER_URL + ReqConst.FUN_GET_TOP_TEN_VIDEOS;
    private Map<String,String> params;

    public GetTop10VideosRequest(String channelId, Response.Listener<String> listener, Response.ErrorListener error) {

        super(Method.POST, url, listener,error);

        params = new HashMap<>();
        params.put(ReqConst.CHANNEL_ID, channelId);
    }

    @Override
    public Map<String, String> getParams() {
        return params;
    }
}
