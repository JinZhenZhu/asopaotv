package com.sts.asopao.Fragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.github.paolorotolo.expandableheightlistview.ExpandableHeightListView;
import com.sts.asopao.APIs.Get_VIDEOS_Request;
import com.sts.asopao.Adapter.VIDEOS_Adapter;
import com.sts.asopao.Base.BaseFragment;
import com.sts.asopao.Common.Commons;
import com.sts.asopao.Common.Constants;
import com.sts.asopao.Common.ReqConst;
import com.sts.asopao.Main.MainActivity;
import com.sts.asopao.Models.VIDEOS_Model;
import com.sts.asopao.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by STS on 12/1/2017.
 */

@SuppressLint("ValidFragment")
public class VIDEOS_Fragment extends BaseFragment{

    MainActivity activity;
    VIDEOS_Adapter mVIDEOS_Adapter;

    ExpandableHeightListView lstVIDEOS;
    ImageView imvPrev, imvNext;
    TextView txvCurrentPageNumber;

    public VIDEOS_Fragment(MainActivity activity) {
        this.activity = activity;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View fragment = inflater.inflate(R.layout.fragment_videos, container, false);

        txvCurrentPageNumber = (TextView)fragment.findViewById(R.id.txvCurrentPageNumber);
        imvPrev = (ImageView)fragment.findViewById(R.id.imvPrev);
        imvPrev.setOnClickListener(this);
        imvNext = (ImageView)fragment.findViewById(R.id.imvNext);
        imvNext.setOnClickListener(this);

        mVIDEOS_Adapter = new VIDEOS_Adapter(activity);
        lstVIDEOS = (ExpandableHeightListView)fragment.findViewById(R.id.lstVIDEOS);
        lstVIDEOS.setAdapter(mVIDEOS_Adapter);
        lstVIDEOS.setExpanded(true);
        lstVIDEOS.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                Commons.selectedVideoId = mVIDEOS_Adapter.getItem(position).getVideoId();
                activity.showVideoMusicalFragment();
            }
        });


        return fragment;
    }

    private void getAllData() {
        final Response.Listener<String> res = new Response.Listener<String>() {
            @Override
            public void onResponse(String json) {
                activity.closeProgress();
                try{
                    JSONObject response = new JSONObject(json);
                    int resultCode = response.getInt(ReqConst.RES_CODE);
                    if (resultCode == ReqConst.CODE_SUCCESS){

                        ArrayList<VIDEOS_Model> mVIDEOS_Model_List =  new ArrayList<>();
                        JSONArray jsonVIDEOS_Model_List =  response.getJSONArray(ReqConst.VIDEOS);

                        for (int i = 0; i < jsonVIDEOS_Model_List.length();  i++){
                            VIDEOS_Model mVIDEOS_Model = new VIDEOS_Model();
                            JSONObject jsonVIDEOS_Model = (JSONObject) jsonVIDEOS_Model_List.get(i);

                            mVIDEOS_Model.setVideoId(jsonVIDEOS_Model.getInt(ReqConst.VIDEO_ID));
                            mVIDEOS_Model.setTitle(jsonVIDEOS_Model.getString(ReqConst.TITLE));
                            mVIDEOS_Model.setNumberOfViews(jsonVIDEOS_Model.getString(ReqConst.NUMBER_OF_VIEWS));
                            mVIDEOS_Model.setVideoType(jsonVIDEOS_Model.getString(ReqConst.VIDEO_TYPE));
                            mVIDEOS_Model.setChannelName(jsonVIDEOS_Model.getString(ReqConst.CHANNEL_NAME));
                            mVIDEOS_Model.setThumbnail(jsonVIDEOS_Model.getString(ReqConst.THUMBNAIL_URL));

                            mVIDEOS_Model_List.add(mVIDEOS_Model);
                        }

                        Commons.mVIDEOS_Model_List.clear();
                        Commons.mVIDEOS_Model_List.addAll(mVIDEOS_Model_List);

                        mVIDEOS_Adapter.refresh(mVIDEOS_Model_List);
                        txvCurrentPageNumber.setText(String.valueOf(Commons.currentPage));

                    }else{
                        activity.showAlertDialog(getString(R.string.server_failed));
                    }

                }catch (JSONException e){
                    activity.showAlertDialog(getString(R.string.server_failed));
                }
            }
        };
        final Response.ErrorListener error = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                activity.closeProgress();
                activity.showAlertDialog(getString(R.string.server_failed));
            }
        };

        activity.showProgress();
        Get_VIDEOS_Request req = new Get_VIDEOS_Request(String.valueOf(Commons.currentPage), res, error);
        req.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue request = Volley.newRequestQueue(activity);
        request.add(req);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.imvPrev:
                if (Commons.currentPage == 1) return;
                Commons.currentPage--;
                getAllData();
                break;
            case R.id.imvNext:
                Commons.currentPage ++;
                getAllData();
                break;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (Commons.mVIDEOS_Model_List.size() > 0){
            mVIDEOS_Adapter.refresh(Commons.mVIDEOS_Model_List);
        }else {
            getAllData();
        }
    }
}
