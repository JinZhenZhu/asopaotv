package com.sts.asopao.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.makeramen.roundedimageview.RoundedImageView;
import com.squareup.picasso.Picasso;
import com.sts.asopao.Models.VideoModel;
import com.sts.asopao.R;

import java.util.ArrayList;

public class ArtistasRecycleViewAdapter extends RecyclerView.Adapter<ArtistasRecycleViewAdapter.ViewHolder> {

    Context context;
    ArrayList<VideoModel> allData = new ArrayList<>();
    LayoutInflater mLayoutInflater;
    ItemClickListener mItemClickListener;

    public ArtistasRecycleViewAdapter(Context context) {
        this.context = context;
        mLayoutInflater = LayoutInflater.from(context);
    }

    public void refresh(ArrayList<VideoModel> data){
        allData.clear();
        allData.addAll(data);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = mLayoutInflater.inflate(R.layout.item_artistas_detail, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.txvTitle.setText(allData.get(position).getTitle());
        if (allData.get(position).getThumbnailUrl().length() > 0){
            Picasso.with(context).load(allData.get(position).getThumbnailUrl()).into(holder.imvThumbnail);
        }
    }

    @Override
    public int getItemCount() {
        return allData.size();
    }

    public void setClickListener (ItemClickListener itemClickListener){
        this.mItemClickListener = itemClickListener;
    }
    public interface ItemClickListener{
        void onItemClick(View view, int position);
    }

    public VideoModel getItem (int position){
        return allData.get(position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        RoundedImageView imvThumbnail;
        TextView txvTitle;

        public ViewHolder(View itemView) {
            super(itemView);
            imvThumbnail = (RoundedImageView)itemView.findViewById(R.id.imvThumbnail);
            txvTitle = (TextView)itemView.findViewById(R.id.txvTitle);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mItemClickListener != null) mItemClickListener.onItemClick(view, getAdapterPosition());
        }
    }
}
