package com.sts.asopao.Main;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.sts.asopao.APIs.SendMessageRequest;
import com.sts.asopao.Base.BaseActivity;
import com.sts.asopao.Common.Commons;
import com.sts.asopao.Common.Constants;
import com.sts.asopao.Common.ReqConst;
import com.sts.asopao.R;

import org.json.JSONException;
import org.json.JSONObject;

public class ContactUsActivity extends BaseActivity {

    TextView txvFirstName, txvLastName, txvEmail;
    EditText edtPhone, edtMessage;
    Button btnSubmit;

    String firstName = "", lastName = "", email = "", phone = "", message = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_us);

        loadLayout();
    }

    private void loadLayout() {
        txvFirstName = (TextView)findViewById(R.id.txvFirstName);
        txvFirstName.setText(Commons.user.getFirstName());

        txvLastName = (TextView)findViewById(R.id.txvLastName);
        txvLastName.setText(Commons.user.getLastName());

        txvEmail = (TextView)findViewById(R.id.txvEmail);
        txvEmail.setText(Commons.user.getEmail());

        edtPhone = (EditText)findViewById(R.id.edtPhone);
        edtMessage = (EditText)findViewById(R.id.edtMessage);
        btnSubmit = (Button)findViewById(R.id.btnSubmit);
        btnSubmit.setOnClickListener(this);
    }

    private boolean valid() {
        firstName = Commons.user.getFirstName();
        lastName = Commons.user.getLastName();
        email = Commons.user.getEmail();

        phone = edtPhone.getText().toString().trim();
        if (phone.length() == 0){
            showAlertDialog("Please input phone number");
            return false;
        }

        message = edtMessage.getText().toString().trim();
        if (message.length() == 0){
            showAlertDialog("Please input message");
            return false;
        }

        return true;
    }

    private void submitMessage() {
        final Response.Listener<String> res = new Response.Listener<String>() {
            @Override
            public void onResponse(String json) {
                closeProgress();
                try{
                    JSONObject response = new JSONObject(json);
                    int resultCode = response.getInt(ReqConst.RES_CODE);
                    if (resultCode == ReqConst.CODE_SUCCESS){
                        showToast("Email sent");
                    }

                }catch (JSONException e){
                    showAlertDialog(getString(R.string.server_failed));
                }
            }
        };
        final Response.ErrorListener error = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                closeProgress();
                showAlertDialog(getString(R.string.server_failed));
            }
        };

        showProgress();
        SendMessageRequest req = new SendMessageRequest(firstName, lastName, email, phone, message, res, error);
        req.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue request = Volley.newRequestQueue(this);
        request.add(req);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnSubmit:
                if (valid()) submitMessage();
                break;
        }
    }
}
