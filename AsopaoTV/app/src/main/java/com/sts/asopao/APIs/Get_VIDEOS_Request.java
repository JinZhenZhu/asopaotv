package com.sts.asopao.APIs;

import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;
import com.sts.asopao.Common.ReqConst;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by STS on 2/9/2017.
 */

public class Get_VIDEOS_Request extends StringRequest {

    private static  final String url =  ReqConst.SERVER_URL + ReqConst.FUN_GET_PLAYER_TAB_DATA;
    private Map<String,String> params;

    public Get_VIDEOS_Request(String pagerNumber, Response.Listener<String> listener, Response.ErrorListener error) {

        super(Method.POST, url, listener,error);

        params = new HashMap<>();
        params.put(ReqConst.PAGER_NUMBER, pagerNumber);
    }

    @Override
    public Map<String, String> getParams() {
        return params;
    }
}
